<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->boolean('payment_type');
            $table->bigInteger('city_id')->unsigned();
            $table->bigInteger('subscription_id')->unsigned()->default(null);
            $table->string('image')->default(null);
            $table->string('note')->default(null);
            $table->string('date');
            $table->string('time');
            $table->string('lat');
            $table->string('lng');
            $table->string('area_num');
            $table->string('area');
            $table->string('block');
            $table->string('street');
            $table->string('house');
            $table->string('floor');
            $table->string('appartment');
            $table->string('directions');
            $table->integer('status')->default(0);
            $table->boolean('accepted')->default(0);
            $table->string('order_number');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');



            $table->foreign('city_id')
                ->references('id')->on('cities')
                ->onDelete('cascade');

            $table->foreign('subscription_id')
                ->references('id')->on('subscriptions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
