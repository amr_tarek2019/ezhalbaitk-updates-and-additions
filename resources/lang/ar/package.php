<?php
return [
    'AddPackage'=>'اضافة باقة',
    'Packages'=>'باقات',
    'CompleteForm'=>'ادخل البيانات',
    'nameenglish'=>'الاسم باللغة الانجليزية',
    'namearabic'=>'الاسم باللغة العربية',
    'period'=>'المدة',
    'price'=>'السعر',
    'total'=>'الاجمالي',
    'Submit'=>'اخضع',
    'editpackage'=>'تعديل باقة',
    'PackagesDataTable'=>'جدول بيانات الباقات',
    'AddNew'=>'اضف جديد',
    'PackagesDetails'=>'تفاصيل الباقات',
    'actions'=>'الاجراءات',
    'edit'=>'تعديل',
    'delete'=>'حذف',
    'changemsgsuccess'=>'تم تغيير حالة المدينة بنجاح',
    'changemsgfailed'=>'حدث خطأ ما',
    'currency'=>'العملة'
];
