<?php
return [
    'notifications'=>'الاشعارات',
    'addnew'=>'اضف جديد',
    'notificationstable'=>'جدول الاشعارات',
    'username'=>'اسم المستخدم',
    'title'=>'العنوان',
    'text_en'=>'نص الاشعار باللغة الانجليزية',
    'text_ar'=>'نص الاشعار باللغة العربية',
    'created'=>'انشئ في',
    'createnotification'=>'انشاء اشعار',
    'addnotification'=>'اضافة جديد',
    'submit'=>'اخضع',
    'cancel'=>'الغاء',
    'delete'=>'حذف'
];
