<?php
return[
    'Add subscription'=>'اضافة خطة',
    'subscriptions'=>'خطة',
    'months'=>'الشهور',
    'price'=>'السعر',
    'package'=>'الباقة',
    'SUBSCRIPTIONS DATATABLE'=>'جدول الخطط',
    'SUBSCRIPTIONS DETAILS'=>'تفاصيل الخطط',
    'edit subscription'=>'تعديل الخطط'
];
