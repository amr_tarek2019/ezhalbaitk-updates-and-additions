<?php
return[
'USERS SUBSCRIPTIONS TABLE'=>'جدول اشتراكات المستخدمين',
'USERS TABLE'=>'جدول المستخدمين',
'user name'=>'اسم المستخدم',
'email'=>'البريد الالكتروني',
'subscription'=>'اشتراك',
'price'=>'السعر',
'status'=>'الحالة',
'no technician assigned yet'=>'لم يتم تعيين فني حتى الآن',
'CATEGORY AND PACKAGE DATA'=>'بيانات الفئة والصفقة',
'TECHNICIAN DATA'=>'بيانات الفني',
'USER DATA'=>'بيانات المستخدم',
'USER SUBSCRIPTION DETAILS'=>'تفاصيل اشتراك المستخدم',
'CATEGORY AND PACKAGE DATA'=>'تفاصيل القسم و الباقة',
    'category'=>'القسم',
    'package'=>'الباقة'



];
