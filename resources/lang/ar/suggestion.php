<?php
return [
    'Name'=>'اسم',
    'email'=>'البريد الإلكتروني',
    'suggestion'=>'اقتراح',
    'CreatedAt'=>'أرسل في',
    'actions'=>'الاجراءات',
    'suggestions'=>'اقتراحات',
    'suggestionsTable'=>'جدول الاقتراحات',
    'back'=>'العودة',
    'read'=>'قراءة الاقتراح',
    'newmessage'=>'رسالة جديدة'
];