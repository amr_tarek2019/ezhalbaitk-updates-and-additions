<?php
return [
    'profile'=>'الصفحة الشخصية',
    'EditAdminProfile'=>'تعديل الصفحة الشخصية',
    'AdminProfile'=>'صفحة المدير الشخصية',
    'Name'=>'الاسم',
    'email'=>'البريد الإلكتروني',
    'image'=>'صورة',
    'password'=>'كلمه السر',
    'submit'=>'رفع',
    'phone'=>'الهاتف'
];