<?php
return[
    'admins'=>'admins',
    'users'=>'users',
    'addnew'=>'add new',
    'email'=>'email',
    'created'=>'created at',
    'updated'=>'updated at',
    'actions'=>'actions',
    'AdminsTable'=>'Admins Table',
    'createadmin'=>'create admin',
    'name'=>'name',
    'StatusSelect'=>'Status Select',
    'userStatusSelect'=>'User Status select',
    'password'=>'password',
    'update'=>'update',
    'adminedit'=>'admin edit ',
    'deletemsg'=>'Are you sure? You want to delete this?',
    'edit'=>'edit',
    'delete'=>'delete',
    'cancel'=>'cancel',
    'print'=>'print',
    'details'=>'details',
    'phone'=>'phone',
    'editadmin'=>'edit admin',
    'admindata'=>'admin data',
    'status'=>'status',
    'successchangestatus'=>'success , status updated successfully',
    'successerrorchangestatus'=>'danger , Something went wrong'
];