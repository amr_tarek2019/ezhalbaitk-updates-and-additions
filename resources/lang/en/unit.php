<?php
return[
    'units DataTable'=>'units DataTable',
    'units'=>'units',
    'unit status changed successfully'=>'unit status changed successfully',
    'Add unit'=>'Add unit',
    'warranty period'=>'warranty period',
    'period'=>'period',
    'warranty text en'=>'warranty text english',
    'warranty text ar'=>'warranty text arabic',
    'UNITS DETAILS' =>'UNITS DETAILS',
    'period in english'=>'period in english',
    'period in arabic'=>'period in arabic',
    'Edit unit'=>'Edit unit'
];
