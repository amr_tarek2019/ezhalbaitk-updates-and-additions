@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('notification.addnotification')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('notification.notifications')}}</li>
                                <li class="breadcrumb-item active">{{trans('notification.addnotification')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @include('dashboard.layouts.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('notification.addnotification')}}</h5>
                        </div>
                        <form class="form theme-form" method="POST" action="{{ route('notifications.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">{{trans('notification.text_en')}}</label>
                                            <div class="col-sm-9">
                                                <textarea required class="form-control" placeholder="{{trans('notification.text_en')}}" name="text_en" id="text_en"  cols="8" rows="8"></textarea>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">{{trans('notification.text_ar')}}</label>
                                            <div class="col-sm-9">
                                                <textarea required class="form-control" placeholder="{{trans('notification.text_ar')}}" name="text_ar" id="text_ar"  cols="8" rows="8"></textarea>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="col-sm-9 offset-sm-3">
                                    <button class="btn btn-primary" type="submit">{{trans('notification.submit')}}</button>
                                    <input class="btn btn-light" type="reset" value="cancel">
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection
