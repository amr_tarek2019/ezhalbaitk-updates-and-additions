@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('notification.notificationstable')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">{{trans('notification.notifications')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    <a href="{{ route('notifications.create') }}" class="btn btn-primary">{{trans('member.addnew')}}</a>
                    @include('dashboard.layouts.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('notification.notificationstable')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('notification.username')}}</th>
                                        <th>{{trans('notification.text_en')}}</th>
                                        <th>{{trans('notification.created')}} </th>
                                        <th>{{trans('admin.actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($notifications as $key=>$notification)

                                        <tr role="row" class="odd">
                                            <td class="sorting_1">{{ $key + 1 }}</td>
                                            <td>{{ $notification->user->name}}</td>
                                            <td>{{ $notification->text_en }}</td>
                                            <td>{{ $notification->created_at }}</td>
                                            <td>

                                                <form id="delete-form-{{ $notification->id }}" action="{{ route('notifications.destroy',$notification->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger" onclick="if(confirm('{{trans('admin.deletemsg')}}')){
                                                    event.preventDefault();
                                                    document.getElementById('delete-form-{{ $notification->id }}').submit();
                                                    }else {
                                                    event.preventDefault();
                                                    }">{{trans('notification.delete')}}</button>
                                            </td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->

            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection
