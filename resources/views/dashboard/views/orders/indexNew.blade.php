@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('reservation.orderstable')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('reservation.orders')}}</li>
                                <li class="breadcrumb-item active">{{trans('reservation.orderstable')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    @include('dashboard.layouts.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('reservation.ordersdata')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('reservation.date')}}</th>
                                        <th>{{trans('reservation.time')}}</th>
                                        <th>{{trans('reservation.areanum')}}</th>
                                        <th>{{trans('reservation.area')}}</th>
                                        <th>{{trans('reservation.block')}}</th>
                                        <th>{{trans('reservation.street')}}</th>
                                        <th>{{trans('reservation.house')}}</th>
                                        <th>{{trans('reservation.floor')}}</th>
                                        <th>{{trans('reservation.appartment')}}</th>
                                        <th>{{trans('reservation.city')}}</th>
                                        <th>{{trans('reservation.ordernumber')}}</th>
                                        <th>{{trans('reservation.createdat')}}</th>
                                        <th>{{trans('reservation.actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($orders as $key=>$order)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{$order->date}}</td>
                                            <td>{{$order->time}}</td>
                                            <td>{{$order->area_num}}</td>
                                            <td>{{$order->area}}</td>
                                            <td>{{$order->block}}</td>
                                            <td>{{$order->street}}</td>
                                            <td>{{$order->house}}</td>
                                            <td>{{$order->floor}}</td>
                                            <td>{{$order->appartment}}</td>
                                            <td>{{$order->city->city_ar}}</td>
                                            <td>{{$order->order_number}}</td>
                                            <td>{{$order->created_at}}</td>
                                            <td>
                                                <a href="{{ route('orders.edit',$order->id) }}" class="btn btn-info">{{trans('reservation.edit')}}</a>
                                                <a href="{{ route('orders.show',$order->id) }}" class="btn btn-success">{{trans('reservation.details')}}</a>
                                                <form id="delete-form-{{ $order->id }}" action="{{ route('orders.destroy',$order->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger" onclick="if(confirm('{{trans('admin.deletemsg')}}')){
                                                    event.preventDefault();
                                                    document.getElementById('delete-form-{{ $order->id }}').submit();
                                                    }else {
                                                    event.preventDefault();
                                                    }">{{trans('reservation.delete')}}</button>

                                                <a href="{{route('orders.invoice',$order->id)}}" class="btn btn-warning" target="_blank">{{trans('reservation.Print')}}</a>

                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection

