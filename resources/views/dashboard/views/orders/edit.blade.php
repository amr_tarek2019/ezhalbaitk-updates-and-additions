@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('reservation.editorder')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('reservation.orders')}}</li>
                                <li class="breadcrumb-item active">{{trans('reservation.editorder')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-xl-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>{{trans('reservation.editorder')}}</h5>
                                </div>
                                <div class="card-body">
                                    <form class="theme-form"  method="POST" action="{{ route('orders.update',$order->id) }}">
                                        <input hidden name="user_id" value="{{$order->user->id}}">
                                       @csrf

{{--                                        <div class="form-group">--}}
{{--                                            <label class="col-form-label pt-0" for="total_price">total</label>--}}
{{--                                            <input class="form-control" name="total_price" id="total_price" value="{{$order->total_price}}" type="text" aria-describedby="emailHelp" placeholder="Enter email">--}}
{{--                                        </div>--}}
                                        <div class="mb-2">
                                            <div class="col-form-label">{{trans('reservation.status')}}</div>
                                            <select class="form-control form-control-primary btn-square" name="accepted">
                                                <option value="0" {{ isset($order) && $order->accepted == 0 ? 'selected'  :'' }}>Refused</option>
                                                <option value="1" {{ isset($order) && $order->accepted == 1 ? 'selected'  :'' }}>Accepted</option>
                                            </select>
                                        </div>

                                        <br>

                                        <div class="mb-2">
                                            <div class="col-form-label">{{trans('reservation.editreservationstatus')}}</div>
                                            <select class="form-control form-control-primary btn-square" name="status">
                                                <option value="0" {{ isset($order) && $order->status == 0 ? 'selected'  :'' }}>pending </option>
                                                <option value="1" {{ isset($order) && $order->status == 1 ? 'selected'  :'' }}>on way</option>
                                                <option value="5" {{ isset($order) && $order->status == 5 ? 'selected'  :'' }}>delivered</option>
                                            </select>
                                        </div>
                                        <br>
                                        <div class="mb-2">
                                            <div class="col-form-label">{{trans('reservation.selecttechnician')}}</div>
                                            <select name="technician" class="form-control form-control-primary btn-square" >
                                                @foreach($technicians as $technician)
                                                    <option value="{{ $technician->id }}">{{ $technician->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <br>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Date</label>
                                            <div class="col-sm-9">
                                                <input class="form-control digits" type="date" name="date" id="date" value="{{$order->date}}">
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Time</label>
                                            <div class="col-sm-9">
                                                <input class="form-control digits" type="time" name="time" id="time" value="{{$order->time}}">
                                            </div>
                                        </div>
                                        <br>
                                        <div >
                                            <button class="btn btn-primary" type="submit">{{trans('reservation.edit')}}</button>
                                            <button class="btn btn-secondary">{{trans('reservation.cancel')}}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>






@endsection
