@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('reservation.orderdata')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html" data-original-title="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
                                <li class="breadcrumb-item">{{trans('reservation.orders')}}</li>
                                <li class="breadcrumb-item active">{{trans('reservation.orderdata')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('reservation.orderdata')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>
                                        <th>{{trans('reservation.payment')}}</th>
                                        <th>{{trans('reservation.city')}}</th>
                                        <th>{{trans('reservation.image')}}</th>
                                        <th>{{trans('reservation.note')}}</th>
                                        <th>{{trans('reservation.date')}}</th>
                                        <th>{{trans('reservation.time')}}</th>
                                        <th>{{trans('reservation.areanum')}}</th>
                                        <th>{{trans('reservation.area')}}</th>
                                        <th>{{trans('reservation.block')}}</th>
                                        <th>{{trans('reservation.street')}}</th>
                                        <th>{{trans('reservation.house')}}</th>
                                        <th>{{trans('reservation.floor')}}</th>
                                        <th>{{trans('reservation.appartment')}}</th>
                                        <th>{{trans('reservation.directions')}}</th>
                                        <th>{{trans('reservation.status')}}</th>
                                        <th>{{trans('reservation.acceptation')}}</th>
                                        <th>{{trans('reservation.packagenameenglish')}}</th>
                                        <th>{{trans('reservation.ordernumber')}}</th>
                                        <th>{{trans('reservation.technician')}}</th>
                                        <th>{{trans('reservation.collected money')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <td>     @if($order->payment == true)
                                            <span class="label label-info">cash</span>
                                        @else
                                            <span class="label label-danger">paypal</span>
                                        @endif</td>
                                    <td>{{$order->city->city_en}}</td>
                                    <td><img class="img-fluid img-60" src="{{ $order->image}}" alt="#" data-original-title="" title=""></td>
                                    <td>@if($order->note != null)
                                            {{$order->note}}
                                        @else
                                            <p>{{trans('reservation.no notes found')}}</p>
                                        @endif
                                    </td>
                                    <td>{{$order->date}}</td>
                                    <td>{{$order->time}}</td>
                                    <td>{{$order->area_num}}</td>
                                    <td>{{$order->area}}</td>
                                    <td>{{$order->block}}</td>
                                    <td>{{$order->street}}</td>
                                    <td>{{$order->house}}</td>
                                    <td>{{$order->floor}}</td>
                                    <td>{{$order->appartment}}</td>
                                    <td>{{$order->directions}}</td>
                                    <td>
                                        @if($order->status == null)
                                            <span class="label label-info">{{trans('reservation.not viewed')}}</span>
                                        @elseif($order->status == 0)
                                            <span class="label label-success">next visit</span>

                                        @elseif($order->status == 1 || $order->status == 2 ||$order->status == 3 ||$order->status == 4)
                                            <span class="label label-success">{{trans('reservation.in progress')}}</span>
                                        @else
                                            <span class="label label-danger">{{trans('reservation.completed')}}</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($order->accepted == true)
                                            <span class="label label-info">{{trans('reservation.accepted')}}</span>
                                        @else
                                            <span class="label label-danger">{{trans('reservation.notaccepted')}}</span>
                                        @endif

                                    </td>
                                    <td>
                                        @if($order->subscription_id == null)
                                            <span class="label label-info">{{trans('reservation.no package ordered')}}</span>
                                        @else
                                            <span class="label label-danger">{{$order->subscription->package->name_en}}</span>
                                        @endif
                                    </td>

                                    <td>{{$order->order_number}}</td>

                                    <td>
                                        @if(!empty($gettechnician))
                                            @foreach($gettechnician as $user)
                                                {{$user->name}}
                                            @endforeach
                                        @else
                                            <span class="label label-danger">technician not assigned yet</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if(!empty($getPrice))
                                            {{$getPrice->price_amount}}
                                        @else
                                            <span class="label label-danger">no money collected yet</span>
                                        @endif
                                    </td>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('user.userDetails')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>

                                        <th>{{trans('user.name')}}</th>
                                        <th>{{trans('user.email')}}</th>
                                        <th>{{trans('user.image')}}</th>
                                        <th>{{trans('user.phone')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>


                                    <td> <a href="{{route('users.show',$order->user_id)}}" data-original-title="" title="">{{ $order->user->name}}</a></td>
                                    <td>{{ $order->user['email']}}</td>
                                    <td><img class="img-fluid img-60" src="{{ $order->user->image}}" alt="#" data-original-title="" title=""></td>
                                    <td>{{ $order->user->phone}}</td>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('reservation.ordersdata')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>
                                        <th>{{trans('reservation.quantity')}}</th>
                                        <th>{{trans('reservation.total')}}</th>
                                    </tr>
                                    </thead>
                                    @foreach($orderDetails as $orderDetail)
                                        <tbody>
                                        <td>{{$orderDetail->quantity}}</td>
                                        <td>{{$orderDetail->total}}</td>
                                        </tbody>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>





                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            {{trans('reservation.subcategory')}}
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>
                                        {{trans('reservation.subcategory')}}
                                    </tr>
                                    </thead>
                                    @foreach($subcategories as $subcategory)
                                        <tbody>
                                        <td>{{$subcategory->name_en}}
                                            <br>
                                            {{$subcategory->name_ar}}
                                        </td>
                                        </tbody>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>






@endsection
