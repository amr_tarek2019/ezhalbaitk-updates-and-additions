@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('unit.Edit unit')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('unit.units')}}</li>
                                <li class="breadcrumb-item active">{{trans('unit.Edit unit')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('category.completeform')}}</h5>
                        </div>
                        <div class="card-body">
                            <form class="needs-validation" novalidate="" action="{{ route('units.update',$unit->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label class="form-label">{{trans('category.titleenglish')}}</label>
                                        <input class="form-control" value="{{$unit->name_en}}" id="name_en" name="name_en" placeholder="Enter About your description"/>
                                    </div>
                                </div>
                                </div>
                                <br>
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label class="form-label">{{trans('category.titlearabic')}}</label>
                                        <input class="form-control" value="{{$unit->name_ar}}" id="name_ar" name="name_ar"  placeholder="Enter About your description"/>
                                    </div>
                                </div>
                                </div>
                                <br>
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="col-form-label">{{trans('technician.category')}}</div>
                                    <select name="category" class="form-control digits">
                                        @foreach($categories as $category)
                                            <option {{ $category->id == $unit->category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name_en }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect9">{{trans('unit.warranty period')}}</label>
                                            <select class="form-control digits" name="warranty_period" id="warranty_period">
                                                <option {{ isset($unit) && $unit->warranty_period == 1 ? 'selected'  :'' }} value="1">1</option>
                                                <option {{ isset($unit) && $unit->warranty_period == 2 ? 'selected'  :'' }} value="2">2</option>
                                                <option {{ isset($unit) && $unit->warranty_period == 3 ? 'selected'  :'' }} value="3">3</option>
                                                <option {{ isset($unit) && $unit->warranty_period == 4 ? 'selected'  :'' }} value="4">4</option>
                                                <option {{ isset($unit) && $unit->warranty_period == 5 ? 'selected'  :'' }} value="5">5</option>
                                                <option {{ isset($unit) && $unit->warranty_period == 6 ? 'selected'  :'' }} value="6">6</option>
                                                <option {{ isset($unit) && $unit->warranty_period == 7 ? 'selected'  :'' }} value="7">7</option>
                                                <option {{ isset($unit) && $unit->warranty_period == 8 ? 'selected'  :'' }} value="8">8</option>
                                                <option {{ isset($unit) && $unit->warranty_period == 9 ? 'selected'  :'' }} value="9">9</option>
                                                <option {{ isset($unit) && $unit->warranty_period == 10 ? 'selected'  :'' }} value="1">10</option>
                                                <option {{ isset($unit) && $unit->warranty_period == 11 ? 'selected'  :'' }} value="11">11</option>
                                                <option {{ isset($unit) && $unit->warranty_period == 12 ? 'selected'  :'' }} value="12">12</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <br>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect9">{{trans('unit.period in english')}}</label>
                                            <select class="form-control digits" name="period_en" id="period_en">
                                                <option {{ isset($unit) && $unit->period_en == "year" ? 'selected'  :'' }} value="year">year</option>
                                                <option {{ isset($unit) && $unit->period_en == "month" ? 'selected'  :'' }}  value="month">month</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect9">{{trans('unit.period in arabic')}}</label>
                                            <select class="form-control digits" name="period_ar" id="period_ar">
                                                <option {{ isset($unit) && $unit->period_ar == "شهر" ? 'selected'  :'' }} value="year">شهر</option>
                                                <option {{ isset($unit) && $unit->period_ar == "سنة" ? 'selected'  :'' }}  value="month">سنة</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group mb-0">
                                            <label for="exampleFormControlTextarea4">{{trans('unit.warranty text en')}}</label>
                                            <textarea class="form-control" id="warranty_text_en" name="warranty_text_en" rows="3">{{$unit->warranty_text_en}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group mb-0">
                                            <label for="exampleFormControlTextarea4">{{trans('unit.warranty text ar')}}</label>
                                            <textarea class="form-control" id="warranty_text_ar" name="warranty_text_ar" rows="3">{{$unit->warranty_text_ar}} </textarea>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div style="margin-left: 15px;">
                                    <img class="img-responsive img-thumbnail" src="{{ asset($unit->image) }}" style="height: 100px; width: 100px;" alt="">
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label class="form-label" style="margin-left: 30px;">{{trans('category.uploadfile')}}</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" id="image" name="image" type="file" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <button class="btn btn-primary" type="submit">{{trans('category.submit')}}</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection
