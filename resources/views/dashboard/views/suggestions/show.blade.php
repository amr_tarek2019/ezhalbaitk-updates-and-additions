@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('suggestion.read')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('suggestion.suggestions')}}  </li>
                                <li class="breadcrumb-item active">{{trans('suggestion.read')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="email-wrap">
                <div class="row">


                    <div class="col-xl-12 col-md-12">
                        <div class="email-right-aside">
                            <div class="card email-body radius-left">
                                <div class="pl-0">
                                    <div class="tab-content">
                                        <div class="tab-pane fade" id="pills-darkhome" role="tabpanel" aria-labelledby="pills-darkhome-tab">
                                            <div class="email-compose">
                                                <div class="email-top compose-border">
                                                    <div class="row">
                                                        <div class="col-sm-8 xl-50">
                                                            <h4 class="mb-0">{{trans('suggestion.newmessage')}}</h4>
                                                        </div>
                                                        <div class="col-sm-4 btn-middle xl-50">
                                                            <button class="btn btn-primary btn-block btn-mail text-center mb-0 mt-0" type="button"><i class="fa fa-paper-plane mr-2"></i> SEND</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="email-wrapper">
                                                    <form class="theme-form">
                                                        <div class="form-group">
                                                            <label class="col-form-label pt-0" for="exampleInputEmail1">To</label>
                                                            <input class="form-control" id="exampleInputEmail1" type="email">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputPassword1">Subject</label>
                                                            <input class="form-control" id="exampleInputPassword1" type="text">
                                                        </div>
                                                        <div class="form-group mb-0">
                                                            <label class="text-muted">Message</label>
                                                            <textarea id="text-box" name="text-box" cols="10" rows="2">                                                            </textarea>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade active show" id="pills-darkprofile" role="tabpanel" aria-labelledby="pills-darkprofile-tab">
                                            <div class="email-content">
                                                <div class="email-top">
                                                    <div class="row">
                                                        <div class="col-md-6 xl-100 col-sm-12">
                                                            <div class="media"><img class="mr-3 rounded-circle" src="../assets/images/user/user.png" alt="">
                                                                <div class="media-body">
                                                                    <h6>
                                                                        <img alt="" width="100px" src="{{$suggestion->user->image}}" class="media-object thumb-sm img-circle">
                                                                        <br>
                                                                        <small><span class="digits">{{$suggestion->user->name}} <span class="digits">
                                                                                    <br> {{$suggestion->created_at}}</span></span></small></h6>
                                                                    <p>{{$suggestion->user->email}}</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="email-wrapper">
                                                    <p>{{$suggestion->problem}}</p>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="text-right">
                                                            <a style="
    margin-right: 20px;
" href="{{route('suggestions.index')}}" type="button" class="btn btn-primary waves-effect waves-light w-md m-b-30"><i class="mdi mdi-reply m-r-10"></i>{{trans('suggestion.back')}}</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection
