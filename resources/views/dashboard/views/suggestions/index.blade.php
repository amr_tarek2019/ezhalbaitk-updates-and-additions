@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('suggestion.suggestionsTable')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('suggestion.suggestions')}}</li>
                                <li class="breadcrumb-item active">{{trans('suggestion.suggestionsTable')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    @include('dashboard.layouts.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('suggestion.suggestionsTable')}} </h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('suggestion.Name')}}</th>
                                        <th>{{trans('suggestion.email')}}</th>
                                        <th>{{trans('suggestion.suggestion')}}</th>
                                        <th>{{trans('suggestion.CreatedAt')}}</th>
                                        <th>{{trans('suggestion.actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($suggestions as $key=>$suggestion)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{$suggestion->user->name}}</td>
                                            <td>{{$suggestion->user->email}}</td>
                                            <td>{{$suggestion->problem}}</td>
                                            <td>{{$suggestion->created_at}}</td>
                                            <td>
                                                <a href="{{ route('suggestions.show',$suggestion->id) }}" class="btn btn-info">{{trans('admin.details')}}</a>

                                                <form id="delete-form-{{ $suggestion->id }}" action="{{ route('suggestions.destroy',$suggestion->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger" onclick="if(confirm('{{trans('admin.deletemsg')}}')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $suggestion->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }">{{trans('admin.delete')}}</button>
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>





@endsection
