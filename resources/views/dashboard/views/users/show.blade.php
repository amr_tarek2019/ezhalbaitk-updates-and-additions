@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('user.userDetails')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('user.users')}}</li>
                                <li class="breadcrumb-item active">{{trans('user.userDetails')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
{{--        <div class="container-fluid">--}}
{{--            <div class="edit-profile">--}}
{{--                <div class="row">--}}

{{--                    <div class="col-lg-12">--}}

{{--                            <div class="card-header">--}}
{{--                                <h4 class="card-title mb-0">{{trans('user.userDetails')}}</h4>--}}
{{--                                <div class="card-options"><a class="card-options-collapse" href="edit-profile.html#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="edit-profile.html#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>--}}
{{--                            </div>--}}
{{--                            <div class="card-body">--}}
{{--                                <div class="row">--}}



{{--                                    <div class="col-md-12">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="form-label">{{trans('user.name')}}</label>--}}
{{--                                            <input class="form-control" readonly value="{{$user->name}}" name="name" id="name" type="text" placeholder="{{trans('user.name')}}">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="col-md-12">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="form-label">{{trans('user.email')}}</label>--}}
{{--                                            <input class="form-control" readonly value="{{$user->email}}" name="email" id="email" type="text" placeholder="{{trans('user.email')}}">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="col-md-12">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="form-label">{{trans('user.phone')}}</label>--}}
{{--                                            <input class="form-control" readonly value="{{$user->phone}}" name="phone" id="phone" type="number" placeholder="{{trans('user.phone')}}">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="col-md-12">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="form-label">{{trans('user.address')}}</label>--}}
{{--                                            <input class="form-control" readonly value="{{$user->address}}" name="address" id="address" type="text" placeholder="{{trans('user.address')}}">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="col-md-12">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="form-label">{{trans('user.created')}}</label>--}}
{{--                                            <input class="form-control" readonly value="{{$user->created_at}}" name="created_at" id="created_at" type="text" placeholder="{{trans('user.created')}}">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}


{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="card-footer text-right">--}}
{{--                                <a href="{{route('users.index')}}" class="btn btn-primary" type="submit">{{trans('user.back')}}</a>--}}
{{--                            </div>--}}

{{--                    </div>--}}

{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}






        <div class="container-fluid">

            <div class="col-sm-12">
                @include('dashboard.layouts.msg')
                <div class="card">
                    <div class="card-header">
                        <h5>Edit user password</h5>
                    </div>
                    <div class="card-body">
                        <form class="needs-validation" novalidate="" method="POST" action="{{ route('user.edit.password',$user->id) }}">
                            @csrf
                            <div class="form-row">
                                <div class="col-md-12 mb-3">
                                    <label for="validationCustom01">password</label>
                                    <input class="form-control" id="password" type="password" name="password" placeholder="password" required="">

                                </div>

                            </div>

                            <button class="btn btn-primary" type="submit">Submit form</button>
                        </form>
                    </div>
                </div>

            </div>





            <div class="row">



                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('user.userDetails')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>
                                        <th scope="col">{{trans('user.image')}}</th>
                                        <th scope="col">{{trans('user.name')}}</th>
                                        <th scope="col">{{trans('user.email')}}</th>
                                        <th scope="col">{{trans('user.phone')}}</th>
                                        <th scope="col">{{trans('user.address')}}</th>
                                        <th scope="col">{{trans('user.created')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>
                                        <td><img class="img-fluid img-60" src="{{$user->image}}" alt="#"></td>
                                        <td>
                                            {{$user->name}}
                                        </td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->phone}}</td>
                                        <td><a target="_blank" href="http://maps.google.com/maps?q=loc:{{$user->address}}"/>{{$user->address}}</td>
                                        <td>{{$user->created_at}}</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>subscriptions history</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive">
                                <table class="table table-bordernone display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th scope="col">package</th>
                                        <th scope="col">category</th>
                                        <th scope="col">price</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($userSubscriptions as $userSubscription)
                                    <tr>


                                        <td>{{ $userSubscription->subscription->package->name_en}}</td>
                                        <td>{{ $userSubscription->subscription->category->name_en}}</td>
                                        <td>{{ $userSubscription->total_price}}</td>


                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>الطلبات الحالية</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive">
                                <table class="table table-bordernone display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>{{trans('reservation.payment')}}</th>
                                        <th>{{trans('reservation.city')}}</th>
                                        <th>{{trans('reservation.image')}}</th>
                                        <th>{{trans('reservation.note')}}</th>
                                        <th>{{trans('reservation.date')}}</th>
                                        <th>{{trans('reservation.time')}}</th>
                                        <th>{{trans('reservation.area')}}</th>
                                        <th>{{trans('reservation.block')}}</th>
                                        <th>{{trans('reservation.street')}}</th>
                                        <th>{{trans('reservation.house')}}</th>
                                        <th>{{trans('reservation.floor')}}</th>
                                        <th>{{trans('reservation.appartment')}}</th>
                                        <th>{{trans('reservation.directions')}}</th>
                                        <th>{{trans('reservation.ordernumber')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($userOrders as $userOrder)
                                        <tr>
                                            <td>
                                                @if($userOrder->payment_type == 0)
                                                    <span class="label label-info">cash</span>
                                                @else
                                                    <span class="label label-danger">payment</span>
                                                @endif
                                            </td>
                                            <td>{{ $userOrder->city->city_en}}</td>
                                            <td>
                                                @if(!empty($userOrder->img))
                                                <img class="img-responsive img-thumbnail" src="{{ asset('uploads/orders/'.$userOrder->img) }}" style="height: 100px; width: 100px" alt=""></td>
                                            @else
                                                <span class="label label-info">there is no images upoloaded</span>
                                            @endif
                                            <td>
                                                @if(!empty($userOrder->note))
                                                {{ $userOrder->note}}
                                            @else
                                                    <span class="label label-info">there is no note</span>
                                            @endif</td>
                                            <td>{{ $userOrder->date}}</td>
                                            <td>{{ $userOrder->time}}</td>
                                            <td>{{ $userOrder->area}}</td>
                                            <td>{{ $userOrder->block}}</td>
                                            <td>{{ $userOrder->street}}</td>
                                            <td>{{ $userOrder->house}}</td>
                                            <td>{{ $userOrder->floor}}</td>
                                            <td>{{ $userOrder->appartment}}</td>
                                            <td>{{ $userOrder->directions}}</td>
                                            <td>{{ $userOrder->order_number}}</td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>الطلبات المنتهية</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive">
                                <table class="table table-bordernone display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>{{trans('reservation.payment')}}</th>
                                        <th>{{trans('reservation.city')}}</th>
                                        <th>{{trans('reservation.image')}}</th>
                                        <th>{{trans('reservation.note')}}</th>
                                        <th>{{trans('reservation.date')}}</th>
                                        <th>{{trans('reservation.time')}}</th>
                                        <th>{{trans('reservation.area')}}</th>
                                        <th>{{trans('reservation.block')}}</th>
                                        <th>{{trans('reservation.street')}}</th>
                                        <th>{{trans('reservation.house')}}</th>
                                        <th>{{trans('reservation.floor')}}</th>
                                        <th>{{trans('reservation.appartment')}}</th>
                                        <th>{{trans('reservation.directions')}}</th>
                                        <th>{{trans('reservation.ordernumber')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($userFinishedOrders as $userFinishedOrder)
                                        <tr>
                                            <td>
                                                @if($userFinishedOrder->payment_type == 0)
                                                    <span class="label label-info">cash</span>
                                                @else
                                                    <span class="label label-danger">payment</span>
                                                @endif
                                            </td>
                                            <td>{{ $userFinishedOrder->city->city_en}}</td>
                                            <td>
                                                @if(!empty($userFinishedOrder->img))
                                                    <img class="img-responsive img-thumbnail" src="{{ asset('uploads/orders/'.$userFinishedOrder->img) }}" style="height: 100px; width: 100px" alt=""></td>
                                            @else
                                                <span class="label label-info">there is no images upoloaded</span>
                                            @endif
                                            <td>
                                                @if(!empty($userFinishedOrder->note))
                                                    {{ $userFinishedOrder->note}}
                                                @else
                                                    <span class="label label-info">there is no note</span>
                                                @endif</td>
                                            <td>{{ $userFinishedOrder->date}}</td>
                                            <td>{{ $userFinishedOrder->time}}</td>
                                            <td>{{ $userFinishedOrder->area}}</td>
                                            <td>{{ $userFinishedOrder->block}}</td>
                                            <td>{{ $userFinishedOrder->street}}</td>
                                            <td>{{ $userFinishedOrder->house}}</td>
                                            <td>{{ $userFinishedOrder->floor}}</td>
                                            <td>{{ $userFinishedOrder->appartment}}</td>
                                            <td>{{ $userFinishedOrder->directions}}</td>
                                            <td>{{ $userFinishedOrder->order_number}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12"  >
                    <div class="card">
                        <div class="card-header">
                            <h5>الطلبات التي بها اشتراكات</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive">
                                <table class="table table-bordernone display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>subscription</th>
                                        <th>{{trans('reservation.payment')}}</th>
                                        <th>{{trans('reservation.city')}}</th>
                                        <th>{{trans('reservation.image')}}</th>
                                        <th>{{trans('reservation.note')}}</th>
                                        <th>{{trans('reservation.date')}}</th>
                                        <th>{{trans('reservation.time')}}</th>
                                        <th>{{trans('reservation.area')}}</th>
                                        <th>{{trans('reservation.block')}}</th>
                                        <th>{{trans('reservation.street')}}</th>
                                        <th>{{trans('reservation.house')}}</th>
                                        <th>{{trans('reservation.floor')}}</th>
                                        <th>{{trans('reservation.appartment')}}</th>
                                        <th>{{trans('reservation.directions')}}</th>
                                        <th>{{trans('reservation.ordernumber')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($userSubscriptionsOrders))
                                    @foreach($userSubscriptionsOrders as $userSubscriptionsOrder)
                                        <tr>
                                            <td>
                                                @if(!empty($userSubscriptionsOrder->subscription->package->name_en))
                                                {{ $userSubscriptionsOrder->subscription->package->name_en}}
                                                @else
                                                    <span class="label label-info">no package found</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if($userSubscriptionsOrder->payment_type == 0)
                                                    <span class="label label-info">cash</span>
                                                @else
                                                    <span class="label label-danger">payment</span>
                                                @endif
                                            </td>
                                            <td>{{ $userSubscriptionsOrder->city->city_en}}</td>
                                            <td>
                                                @if(!empty($userSubscriptionsOrder->img))
                                                    <img class="img-responsive img-thumbnail" src="{{ asset('uploads/orders/'.$userSubscriptionsOrder->img) }}" style="height: 100px; width: 100px" alt=""></td>
                                            @else
                                                <span class="label label-info">there is no images upoloaded</span>
                                            @endif
                                            <td>
                                                @if(!empty($userSubscriptionsOrder->note))
                                                    {{ $userSubscriptionsOrder->note}}
                                                @else
                                                    <span class="label label-info">there is no note</span>
                                                @endif</td>
                                            <td>{{ $userSubscriptionsOrder->date}}</td>
                                            <td>{{ $userSubscriptionsOrder->time}}</td>
                                            <td>{{ $userSubscriptionsOrder->area}}</td>
                                            <td>{{ $userSubscriptionsOrder->block}}</td>
                                            <td>{{ $userSubscriptionsOrder->street}}</td>
                                            <td>{{ $userSubscriptionsOrder->house}}</td>
                                            <td>{{ $userSubscriptionsOrder->floor}}</td>
                                            <td>{{ $userSubscriptionsOrder->appartment}}</td>
                                            <td>{{ $userSubscriptionsOrder->directions}}</td>
                                            <td>{{ $userSubscriptionsOrder->order_number}}</td>
                                        </tr>
                                    @endforeach
                                    @else
                                        <span class="label label-info">no subscription found</span>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>جدول المبالغ التي تم دفعها</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1" style="
    width: 100%;
">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>رقم الطلب</th>
                                        <th>المبلغ المدفوع</th>
                                        <th>طريقة الدفع</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($priceAmounts as $key => $priceAmount)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$priceAmount->order->order_number}}</td>
                                        <td>{{$priceAmount->price_amount}}</td>
                                        <td>
                                            @if($priceAmount->order->payment_type == 1)
                                                <span class="label label-info">visa</span>
                                            @else
                                                <span class="label label-danger">cash</span>
                                            @endif

                                        </td>
                                    </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>


@endsection
