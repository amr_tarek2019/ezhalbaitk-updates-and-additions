@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('settings.settings')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">{{trans('settings.settings')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="edit-profile">
                <div class="row">
                    <div class="col-lg-12">
                        @include('dashboard.layouts.msg')
                        <form class="card" action="{{route('settings.update')}}" method="POST"  enctype="multipart/form-data">
                            @csrf
                            <div class="card-header">
                                <h4 class="card-title mb-0">{{trans('settings.settings')}}</h4>
                                <div class="card-options"><a class="card-options-collapse" href="edit-profile.html#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="edit-profile.html#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                            </div>
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group mb-0">
                                            <label class="form-label">{{trans('settings.aboutE')}}</label>
                                            <textarea class="form-control"
                                                      name="text_en" id="text_en"
                                                      rows="5" placeholder="{{trans('settings.aboutE')}}">{{$settings->text_en}}</textarea>
                                        </div>
                                    </div>
                                    <br><br><br><br><br><br><br><br><br><br>

                                    <div class="col-md-12">
                                        <div class="form-group mb-0">
                                            <label class="form-label">{{trans('settings.aboutA')}}</label>
                                            <textarea class="form-control" rows="5"
                                                      name="text_ar" id="text_ar"
                                                      placeholder="{{trans('settings.aboutA')}}">{{$settings->text_ar}}</textarea>
                                        </div>
                                    </div>
                                    <br><br><br><br><br><br><br><br><br>
                                    <div class="col-md-12">
                                        <div class="form-group mb-0">
                                            <label class="form-label">{{trans('settings.facebook')}}</label>
                                            <input class="form-control" type="text" name="facebook" id="facebook"
                                                      placeholder="{{trans('settings.facebook')}}"
                                                    value="{{$settings->facebook}}"/>
                                        </div>
                                    </div>
                                    <br><br><br><br><br>
                                    <div class="col-md-12">
                                        <div class="form-group mb-0">
                                            <label class="form-label">{{trans('settings.google')}}</label>
                                            <input class="form-control" type="text" name="google" id="google"
                                                   placeholder="{{trans('settings.google')}}"
                                                   value="{{$settings->google}}"/>
                                        </div>
                                    </div>
                                    <br><br><br><br><br>

                                        <div class="col-md-12">
                                            <div class="form-group mb-0">
                                                <label for="exampleFormControlTextarea4">terms and conditions in english</label>
                                                <textarea class="form-control" id="terms_conditions_en" name="terms_conditions_en" rows="3">{{$settings->terms_conditions_en}}</textarea>
                                            </div>
                                        </div>

                                    <br><br><br><br><br><br>

                                        <div class="col-md-12">
                                            <div class="form-group mb-0">
                                                <label for="exampleFormControlTextarea4">terms and conditions in arabic</label>
                                                <textarea class="form-control" id="terms_conditions_ar" name="terms_conditions_ar" rows="3">{{$settings->terms_conditions_ar}}</textarea>
                                            </div>
                                        </div>

                                    <br><br><br><br><br><br>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">{{trans('settings.logo')}}</label>
                                    <img class="img-fluid rounded" src="{{ asset($settings->image) }}" itemprop="thumbnail" alt="gallery" data-original-title="" title=""
                                         style="margin-left: -200px;margin-top: 20px;max-width: 100px;">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">{{trans('settings.logo')}}</label>
                                            <div class="col-sm-12">
                                                <input class="form-control" type="file" data-original-title="" title="" id="image" name="image" >
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-primary" type="submit">{{trans('settings.submit')}}</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>


@endsection
