@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('profile.AdminProfile')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('profile.profile')}}</li>
                                <li class="breadcrumb-item active">{{trans('profile.EditAdminProfile')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="edit-profile">
                <div class="row">

                    <div class="col-lg-12">
                        @include('dashboard.layouts.msg')

                        <form class="card" method="POST" action="{{ route('profile.update',Auth::user()->id) }}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-header">
                                <h4 class="card-title mb-0">{{trans('profile.EditAdminProfile')}}</h4>
                                <div class="card-options"><a class="card-options-collapse" href="edit-profile.html#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="edit-profile.html#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                            </div>
                            <div class="card-body">
                                <div class="row">



                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">{{trans('user.name')}}</label>
                                            <input class="form-control" name="name" id="name" value="{{$user->name}}" type="text" placeholder="{{trans('user.name')}}">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">{{trans('user.email')}}</label>
                                            <input class="form-control" value="{{$user->email}}"  name="email" id="email" type="text" placeholder="{{trans('user.email')}}">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">{{trans('profile.phone')}}</label>
                                            <input class="form-control" value="{{$user->phone}}"  name="phone" id="phone" type="text" placeholder="{{trans('profile.phone')}}">
                                        </div>
                                    </div>

                                    <img class="img-fluid rounded" src="{{ asset($user->image) }}" itemprop="thumbnail" alt="gallery" data-original-title="" title=""
                                         style="margin-left: 15px;max-width: 100px;">

                                    <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">{{trans('category.uploadfile')}}</label>
                                                <div class="col-sm-12">
                                                    <input class="form-control" type="file" data-original-title="" title="" id="image" name="image" >
                                                </div>
                                            </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">{{trans('user.password')}}</label>
                                            <input class="form-control" type="text" placeholder="{{trans('user.password')}}" name="password" id="password">
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-primary" type="submit">{{trans('profile.submit')}}</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection
