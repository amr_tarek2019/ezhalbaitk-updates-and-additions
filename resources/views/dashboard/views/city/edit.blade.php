@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('city.editcity')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('city.cities')}}</li>
                                <li class="breadcrumb-item active">{{trans('city.editcity')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>complete form</h5>
                        </div>
                        <div class="card-body">
                            <form class="needs-validation" novalidate="" action="{{ route('cities.update',$city->id) }}" method="POST" >
                                @csrf
                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label class="form-label">{{trans('city.nameenglish')}}</label>
                                        <input class="form-control" id="city_en" value="{{$city->city_en}}" name="city_en" placeholder="{{trans('city.nameenglish')}}"/>
                                    </div>
                                </div>
                                <br>
                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label class="form-label">{{trans('city.namearabic')}}</label>
                                        <input class="form-control" id="city_ar" value="{{$city->city_ar}}" name="city_ar"  placeholder="{{trans('city.namearabic')}}"/>
                                    </div>
                                </div>
                                <br>
                                <button class="btn btn-primary" type="submit">{{trans('city.submitForm')}}</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection
