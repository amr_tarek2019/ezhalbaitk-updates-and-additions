@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('city.citiestable')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('city.cities')}}</li>
                                <li class="breadcrumb-item active">{{trans('city.citiestable')}}</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <a href="{{ route('cities.create') }}" class="btn btn-primary">{{trans('city.addnew')}}</a>
                    @include('dashboard.layouts.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('city.citiesdetails')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('city.nameenglish')}}</th>
                                        <th>{{trans('city.namearabic')}}</th>
                                        <th>{{trans('city.status')}}</th>
                                        <th width="10%">{{trans('city.actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($cities as $key => $city)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$city->city_en}}</td>
                                            <td>{{$city->city_ar}}</td>
                                            <td>
                                                <div class="media-body text-left icon-state">
                                                    <label class="switch">
                                                        <input onchange="updateCityStatus(this)" value="{{ $city->id }}" type="checkbox"
                                                        <?php if($city->status == 1) echo "checked";?>>
                                                        <span class="switch-state bg-primary"></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="{{ route('cities.edit',$city->id) }}" class="btn btn-info"><i class="material-icons">{{trans('city.edit')}}</i></a>

                                                <form id="delete-form-{{ $city->id }}" action="{{ route('cities.destroy',$city->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger" onclick="if(confirm('{{trans('category.deletemsg')}}')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $city->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }"><i class="material-icons">{{trans('city.delete')}}</i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('myjsfile')
    <script>
        function updateCityStatus(elUser){
            if(elUser.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('cities.status',isset($city) ? $city->id : "") }}', {_token:'{{ csrf_token() }}', id:elUser.value, status:status}, function(data){
                if(data == 1){
                    alert('{{trans('city.changemsgsuccess')}}');
                }
                else{
                    alert('{{trans('category.errorchangestatus')}}');
                }
            });
        }
    </script>
@endsection
