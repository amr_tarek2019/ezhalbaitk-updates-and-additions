@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('member.memberstable')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('member.members')}}</li>
                                <li class="breadcrumb-item active">{{trans('member.memberstable')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    <a href="{{ route('members.create') }}" class="btn btn-primary">{{trans('member.addnew')}}</a>
                    @include('dashboard.layouts.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('member.membersdata')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('admin.name')}}</th>
                                        <th>{{trans('admin.email')}}</th>
                                        <th>{{trans('admin.status')}}</th>
                                        <th>{{trans('admin.created')}}</th>
                                        <th>{{trans('admin.updated')}}</th>
                                        <th>{{trans('admin.actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($users as $key=>$user)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>
                                                <div class="media-body text-right icon-state">
                                                <label class="switch">
                                                    <input onchange="updateMemberStatus(this)" value="{{ $user->id }}" type="checkbox"
                                                    <?php if($user->user_status == 1) echo "checked";?>>
                                                    <span class="switch-state bg-primary"></span>
                                                </label>
                                                </div>
                                            </td>
                                            <td>{{$user->created_at}}</td>
                                            <td>{{$user->created_at}}</td>
                                            <td>
                                                <a href="{{ route('members.edit',$user->id) }}" class="btn btn-info">{{trans('admin.edit')}}</a>

                                                <form id="delete-form-{{ $user->id }}" action="{{ route('members.destroy',$user->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger" onclick="if(confirm('{{trans('admin.deletemsg')}}')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $user->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }">{{trans('admin.delete')}}</button>
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection
    @section('myjsfile')
    <script>
        function updateMemberStatus(elUser){
            if(elUser.checked){
                var user_status = 1;
            }
            else{
                var user_status = 0;
            }
            $.post('{{ route('members.status', isset($user) ? $user->id : "") }}', {_token:'{{ csrf_token() }}', id:elUser.value, user_status:user_status}, function(data){
                if(data == 1){
                    alert('{{trans('member.successchangestatus')}}');
                }
                else{
                    alert('{{trans('member.successerrorchangestatus')}}');
                }
            });
        }
    </script>
@endsection
