@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('member.updatemember')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('member.members')}}</li>
                                <li class="breadcrumb-item active">{{trans('member.updatemember')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="edit-profile">
                <div class="row">

                    <div class="col-lg-12">
                        <form class="card" method="POST" action="{{ route('members.update',$user->id) }}">
                            @csrf
                            <div class="card-header">
                                <h4 class="card-title mb-0">{{trans('member.updatemember')}}</h4>
                                <div class="card-options"><a class="card-options-collapse" href="edit-profile.html#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="edit-profile.html#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                            </div>
                            <div class="card-body">
                                <div class="row">



                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">{{trans('member.name')}}</label>
                                            <input class="form-control" value="{{$user->name}}" name="name" id="name" type="text" placeholder="{{trans('member.name')}}">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">{{trans('member.email')}}</label>
                                            <input class="form-control" value="{{$user->email}}" name="email" id="email" type="text" placeholder="{{trans('member.email')}}">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">{{trans('member.phone')}}</label>
                                            <input class="form-control"  value="{{$user->phone}}" name="phone" id="phone" type="text" placeholder="{{trans('member.phone')}}">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">{{trans('member.password')}}</label>
                                            <input class="form-control" type="password" placeholder="{{trans('member.password')}}" name="password" id="password">
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-primary" type="submit">{{trans('member.submit')}}</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>


@endsection
