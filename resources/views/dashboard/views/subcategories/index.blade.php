@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('subcategory.SubcategoriesDataTable')}} </h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('subcategory.Subcategories')}}</li>
                                <li class="breadcrumb-item active">{{trans('subcategory.SubcategoriesDataTable')}}</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <a href="{{ route('subcategories.create') }}" class="btn btn-primary">{{trans('member.addnew')}}</a>
                    @include('dashboard.layouts.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('subcategory.SubcategoriesDetails')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('subcategory.nameenglish')}}</th>
                                        <th>{{trans('subcategory.namearabic')}}</th>
                                        <th>{{trans('subcategory.price')}}</th>
                                        <th>{{trans('subcategory.status')}}</th>
                                        <th width="10%">{{trans('user.actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($subcategories as $key => $subcategory)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$subcategory->name_en}}</td>
                                            <td>{{$subcategory->name_ar}}</td>
                                            <td>{{$subcategory->price}}</td>
                                            <td>
                                                <div class="media-body text-left switch-lg icon-state">
                                                    <label class="switch">
                                                        <input onchange="updateSubcategoryStatus(this)" value="{{ $subcategory->id }}" type="checkbox"
                                                        <?php if($subcategory->status == 1) echo "checked";?>>
                                                        <span class="switch-state bg-primary"></span>
                                                    </label>
                                                </div>
                                            </td>


                                            <td>
                                                <a href="{{ route('subcategories.edit',$subcategory->id) }}" class="btn btn-info btn-sm"><i class="material-icons">{{trans('admin.edit')}}</i></a>

                                                <form id="delete-form-{{ $subcategory->id }}" action="{{ route('subcategories.destroy',$subcategory->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger btn-sm" onclick="if(confirm('{{trans('user.deletemsg')}}')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $subcategory->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }"><i class="material-icons">{{trans('admin.delete')}}</i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('myjsfile')
    <script>
    function updateSubcategoryStatus(ell){
    if(ell.checked){
    var status = 1;
    }
    else{
    var status = 0;
    }
    $.post('{{ route('subcategories.status',isset($subcategory) ? $subcategory->id : "") }}', {_token:'{{ csrf_token() }}', id:ell.value, status:status}, function(data){
        if(data == 1){
            alert('{{trans('admin.successchangestatus')}}');
        }
        else{
            alert('{{trans('admin.successerrorchangestatus')}}');
        }
    });
    }

    </script>
    @endsection
