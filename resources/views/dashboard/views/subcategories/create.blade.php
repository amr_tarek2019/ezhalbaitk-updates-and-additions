@extends('dashboard.layouts.master')
@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>{{trans('subcategory.AddSubcategory')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">{{trans('subcategory.Subcategories')}}</li>
                            <li class="breadcrumb-item active">{{trans('subcategory.AddSubcategory')}}</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{trans('subcategory.CompleteForm')}}</h5>
                    </div>
                    <div class="card-body">
                        <form class="needs-validation" novalidate="" action="{{ route('subcategories.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="col-md-12">
                                <div class="form-group mb-0">
                                    <label class="form-label">{{trans('subcategory.nameenglish')}}</label>
                                    <input class="form-control" id="name_en" name="name_en" placeholder="{{trans('subcategory.nameenglish')}}"/>
                                </div>
                            </div>
                            <br>
                            <div class="col-md-12">
                                <div class="form-group mb-0">
                                    <label class="form-label">{{trans('subcategory.namearabic')}}</label>
                                    <input class="form-control" id="name_ar" name="name_ar"  placeholder="{{trans('subcategory.namearabic')}}"/>
                                </div>
                            </div>
                            <br>

                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label for="exampleFormControlTextarea4">{{trans('subcategory.englishdetails')}}</label>
                                        <textarea class="form-control" id="details_en" name="details_en" rows="3"></textarea>
                                    </div>
                                </div>

                            <br>

                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label for="exampleFormControlTextarea4">{{trans('subcategory.arabicdetails')}}</label>
                                        <textarea class="form-control" id="details_ar" name="details_ar" rows="3"></textarea>
                                    </div>
                                </div>

                            <br>
                            <div class="col-md-12">
                                <div class="col-form-label">{{trans('subcategory.unit')}}</div>
                                <select name="unit" class="js-example-placeholder-multiple col-sm-12">
                                    @foreach($units as $unit)
                                        <option value="{{ $unit->id }}">{{ $unit->name_en }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                            <div class="col-md-12">
                                <div class="form-group mb-0">
                                    <label class="form-label">{{trans('subcategory.price')}}</label>
                                    <input class="form-control" id="price" name="price"  placeholder="{{trans('subcategory.price')}}"/>
                                </div>
                            </div>
                            <br>
                         <button class="btn btn-primary" type="submit">{{trans('subcategory.Submitform')}}</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>
@endsection
