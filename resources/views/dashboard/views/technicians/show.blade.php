@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>Technician Details</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">Technicians</li>
                                <li class="breadcrumb-item active">Technician Details</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">




                    <div class="card">
                        <div class="card-header">
                            <h5>Technician Details</h5>
                        </div>
                        <form class="form theme-form">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Name</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" readonly value="{{$user->name}}" name="name" id="name" type="text" placeholder="{{trans('technician.name')}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Email</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" readonly value="{{$user->email}}" name="email" id="email" type="text" placeholder="{{trans('technician.email')}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">created at</label>
                                            <div class="col-sm-9">
                                            <input class="form-control" readonly value="{{$user->created_at}}" name="phone" id="phone" type="text" placeholder="{{trans('admin.created')}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">phone</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" readonly value="{{$user->phone}}" name="phone" id="phone" type="text" placeholder="{{trans('technician.phone')}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">address</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" readonly value="{{$user->address}}" name="address" id="address" type="text" placeholder="{{trans('technician.address')}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">category</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" readonly value="{{$technician->category->name_en}}" name="category" id="category" type="text" placeholder="{{trans('technician.category')}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>


                </div>

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Collected Prices</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>month</th>
                                        <th>Total Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($technicianCollectedMoney as $collectedMoney)
                                    <tr>
                                        <td>{{$collectedMoney->month}} {{$collectedMoney->year}}</td>
                                        <td> {{$collectedMoney->sum}} QR</td>
                                    </tr>
                                    @endforeach




                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>user requests</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1" style="width: 100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>category</th>
                                        <th>price</th>
                                        <th>user name</th>
                                        <th>date</th>
                                        <th>order number</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($getTechnicianOrders as $key=>$getTechnicianOrder)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>
                                            @foreach($getCategories as $getCategory)
                                            {{$getCategory->name_en}}
                                        @endforeach</td>
                                        <td>
                                            {{$getTotalPrice->sum('total')}}
                                        </td>
                                        <td>@if(!empty($getTechnicianOrder->order->user->name))
                                                {{$getTechnicianOrder->order->user->name}}
                                        @else
                                                <h6>order not found</h6>
                                                @endif
                                        </td>
                                        <td>
                                            @if(!empty($getTechnicianOrder->order->date))
                                                {{$getTechnicianOrder->order->date}}
                                            @else
                                                <h6>order not found</h6>
                                            @endif
                                            </td>
                                        <td>{{$getTechnicianOrder->order_number}}</td>
                                    </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>subscriptions orders</h5>

                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1" style="width: 100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>package</th>
                                        <th>subscription number</th>
                                        <th>price</th>
                                        <th>time</th>
                                        <th>date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($getTechnicianSubscriptions as $key=> $getTechnicianSubscription)

                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$getTechnicianSubscription->userSubscription->subscription->package->name_en}}</td>
                                        <td>{{$getTechnicianSubscription->userSubscription->subscription_number}}</td>
                                        <td>{{$getTechnicianSubscription->userSubscription->total_price}}</td>
                                        <td>{{$getTechnicianSubscription->userSubscription->time}}</td>
                                        <td>{{$getTechnicianSubscription->userSubscription->date}}</td>
                                    </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection
