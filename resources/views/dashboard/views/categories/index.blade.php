@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('category.categoriesdatatable')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('category.categories')}}</li>
                                <li class="breadcrumb-item active">{{trans('category.categoriesdatatable')}}</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <a href="{{ route('categories.create') }}" class="btn btn-primary">{{trans('category.AddNew')}}</a>
                    @include('dashboard.layouts.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('category.categoriesdata')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('category.titleenglish')}}</th>
                                        <th>{{trans('category.titlearabic')}}</th>
                                        <th>{{trans('category.image')}}</th>
                                        <th>{{trans('category.status')}}</th>
                                        <th width="10%">{{trans('category.actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($categories as $key => $category)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$category->name_en}}</td>
                                            <td>{{$category->name_ar}}</td>
                                            <td>
                                                <img class="img-responsive img-thumbnail" src="{{ asset($category->icon) }}" style="height: 100px; width: 100px" alt="">
                                            </td>
                                            <td>
                                                <div class="media-body text-left icon-state">
                                                    <label class="switch">
                                                        <input onchange="update_featured(this)" value="{{ $category->id }}" type="checkbox"
                                                        <?php if($category->status == 1) echo "checked";?>>
                                                        <span class="switch-state bg-primary"></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="{{ route('categories.edit',$category->id) }}" class="btn btn-info btn-sm"><i class="material-icons">{{trans('category.edit')}}</i></a>

                                                <form id="delete-form-{{ $category->id }}" action="{{ route('categories.destroy',$category->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger btn-sm" onclick="if(confirm('{{trans('category.deletemsg')}}')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $category->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }"><i class="material-icons">{{trans('category.delete')}}</i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('myjsfile')
    <script>
    function update_featured(el){
    if(el.checked){
    var status = 1;
    }
    else{
    var status = 0;
    }
    $.post('{{ route('categories.status',$category->id) }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
    if(data == 1){
    alert('{{trans('category.successchangestatus')}}');
    }
    else{
    alert('{{trans('category.errorchangestatus')}}');
    }
    });
    }
    </script>
    @endsection
