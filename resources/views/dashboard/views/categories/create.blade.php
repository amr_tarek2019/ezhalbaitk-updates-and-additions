@extends('dashboard.layouts.master')
@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>{{trans('category.addcategory')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">{{trans('category.categories')}}</li>
                            <li class="breadcrumb-item active">{{trans('category.addcategory')}}</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{trans('category.completeform')}}</h5>
                    </div>
                    <div class="card-body">
                        <form class="needs-validation" novalidate="" action="{{ route('categories.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="col-md-12">
                                <div class="form-group mb-0">
                                    <label class="form-label">{{trans('category.titleenglish')}}</label>
                                    <input class="form-control" id="name_en" name="name_en" placeholder="{{trans('category.titleenglish')}}"/>
                                </div>
                            </div>
                            <br>
                            <div class="col-md-12">
                                <div class="form-group mb-0">
                                    <label class="form-label">{{trans('category.titlearabic')}}</label>
                                    <input class="form-control" id="name_ar" name="name_ar"  placeholder="{{trans('category.titlearabic')}}"/>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="form-label" style="margin-left: 30px;">{{trans('category.uploadfile')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" id="icon" name="icon" type="file" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                         <button class="btn btn-primary" type="submit">{{trans('category.submit')}}</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>
@endsection
