{{--@extends('dashboard.layouts.master')--}}

{{--@section('content')--}}

{{--<div class="col-lg-6 col-lg-offset-3">--}}
{{--    <div class="panel">--}}
{{--        <div class="panel-heading">--}}
{{--            <h3 class="panel-title">{{__('Staff Information')}}</h3>--}}
{{--        </div>--}}

{{--        <!--Horizontal Form-->--}}
{{--        <!--===================================================-->--}}
{{--        <form class="form-horizontal" action="{{ route('staffs.store') }}" method="POST" enctype="multipart/form-data">--}}
{{--        	@csrf--}}
{{--            <div class="panel-body">--}}
{{--                <div class="form-group">--}}
{{--                    <label class="col-sm-3 control-label" for="name">{{__('Name')}}</label>--}}
{{--                    <div class="col-sm-9">--}}
{{--                        <input type="text" placeholder="{{__('Name')}}" id="name" name="name" class="form-control" required>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="form-group">--}}
{{--                    <label class="col-sm-3 control-label" for="email">{{__('Email')}}</label>--}}
{{--                    <div class="col-sm-9">--}}
{{--                        <input type="text" placeholder="{{__('Email')}}" id="email" name="email" class="form-control" required>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="form-group">--}}
{{--                    <label class="col-sm-3 control-label" for="password">{{__('Password')}}</label>--}}
{{--                    <div class="col-sm-9">--}}
{{--                        <input type="password" placeholder="{{__('Password')}}" id="password" name="password" class="form-control" required>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="form-group">--}}
{{--                    <label class="col-sm-3 control-label" for="name">{{__('Role')}}</label>--}}
{{--                    <div class="col-sm-9">--}}
{{--                        <select name="role_id" required class="form-control demo-select2-placeholder">--}}
{{--                            @foreach($roles as $role)--}}
{{--                                <option value="{{$role->id}}">{{$role->name}}</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="panel-footer text-right">--}}
{{--                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>--}}
{{--            </div>--}}
{{--        </form>--}}
{{--        <!--===================================================-->--}}
{{--        <!--End Horizontal Form-->--}}

{{--    </div>--}}
{{--</div>--}}

{{--@endsection--}}




@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('staff.Edit Staff')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('staff.staffs')}}</li>
                                <li class="breadcrumb-item active">{{trans('staff.Edit Staff')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('category.completeform')}}</h5>
                        </div>
                        <div class="card-body">
                            <form class="needs-validation" novalidate="" action="{{ route('staffs.update', $staff->id) }}" method="POST" enctype="multipart/form-data">
                                <input name="_method" type="hidden" value="PATCH">
                                @csrf
                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label class="form-label">{{trans('user.name')}}</label>
                                        <input class="form-control" value="{{ $staff->user->name }}" id="name" name="name" placeholder="name"/>
                                    </div>
                                </div>
                                <br>
                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label class="form-label">{{trans('user.email')}}</label>
                                        <input class="form-control" value="{{ $staff->user->email }}" id="email" name="email"  placeholder="email"/>
                                    </div>
                                </div>
                                <br>

                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label for="exampleFormControlTextarea4">{{trans('user.password')}}</label>
                                        <input class="form-control" id="Password" name="Password"  placeholder="Password"/>
                                    </div>
                                </div>
                                <br>
                                <div class="col-md-12">
                                    <div class="col-form-label">{{trans('staff.Role')}}</div>
                                    <select name="role_id" required class="form-control demo-select2-placeholder">
                                        @foreach($roles as $role)
                                            <option value="{{$role->id}}" @php if($staff->role_id == $role->id) echo "selected"; @endphp >{{$role->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <br>
                                <button class="btn btn-primary" type="submit">{{trans('user.submit')}}</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection
