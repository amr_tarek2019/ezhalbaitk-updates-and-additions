@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('rates.TechnicianRateDetails')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html" data-original-title="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
                                <li class="breadcrumb-item">{{trans('rates.Rates')}}</li>
                                <li class="breadcrumb-item">{{trans('rates.Technicians')}}</li>
                                <li class="breadcrumb-item active">{{trans('rates.TechnicianRateDetails')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('rates.TechnicianRateData')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('rates.ordernumber')}}</th>
                                        <th>{{trans('rates.rate')}}</th>
                                        <th>{{trans('rates.CreatedAt')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <td>{{$technicianRate->id}}</td>
                                    <td>{{$technicianRate->order->order_number}}</td>
                                    <td>{{$technicianRate->rate}}</td>
                                    <td>{{$technicianRate->created_at}}</td>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('rates.UserData')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>
                                        <th>{{trans('rates.name')}}</th>
                                        <th>{{trans('rates.email')}}</th>
                                        <th>{{trans('rates.image')}}</th>
                                        <th>{{trans('rates.phone')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>


                                    <td> <a href="{{route('users.show',$technicianRate->order->user->id)}}" data-original-title="" title="">{{ $technicianRate->order->user->name}}</a></td>
                                    <td>{{ $technicianRate->order->user->email}}</td>
                                    <td><img class="img-fluid img-60" src="{{ $technicianRate->order->user->image}}" alt="#" data-original-title="" title=""></td>
                                    <td>{{ $technicianRate->order->user->phone}}</td>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('rates.TechnicianData')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>
                                        <th>{{trans('rates.name')}}</th>
                                        <th>{{trans('rates.email')}}</th>
                                        <th>{{trans('rates.image')}}</th>
                                        <th>{{trans('rates.phone')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>


                                    <td> <a href="{{route('users.show',$technicianRate->technician->user->id)}}" data-original-title="" title="">{{ $technicianRate->technician->user->name}}</a></td>
                                    <td>{{ $technicianRate->technician->user->email}}</td>
                                    <td><img class="img-fluid img-60" src="{{ $technicianRate->technician->user->image}}" alt="#" data-original-title="" title=""></td>
                                    <td>{{ $technicianRate->technician->user->phone}}</td>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection
