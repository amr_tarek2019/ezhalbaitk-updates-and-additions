@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('package.PackagesDataTable')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('package.Packages')}}</li>
                                <li class="breadcrumb-item active">{{trans('package.PackagesDataTable')}}</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <a href="{{ route('packages.create') }}" class="btn btn-primary">{{trans('package.AddNew')}}</a>
                    @include('dashboard.layouts.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('package.PackagesDetails')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('package.nameenglish')}}</th>
                                        <th>{{trans('package.namearabic')}}</th>
                                        <th>{{trans('category.status')}}</th>
                                        <th width="10%">{{trans('package.actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($packages as $key => $package)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$package->name_en}}</td>
                                            <td>{{$package->name_ar}}</td>
                                            <td>
                                                <div class="media-body text-left switch-lg icon-state">
                                                    <label class="switch">
                                                        <input onchange="updateStatus(this)" value="{{ $package->id }}" type="checkbox"
                                                        <?php if($package->status == 1) echo "checked";?>>
                                                        <span class="switch-state bg-primary"></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="{{ route('packages.edit',$package->id) }}" class="btn btn-info btn-sm"><i class="material-icons">{{trans('package.edit')}}</i></a>

                                                <form id="delete-form-{{ $package->id }}" action="{{ route('packages.destroy',$package->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger btn-sm" onclick="if(confirm('{{trans('category.deletemsg')}}')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $package->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }"><i class="material-icons">{{trans('package.delete')}}</i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('myjsfile')
    <script>
        function updateStatus(ell){
            if(ell.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('packages.status',$package->id) }}', {_token:'{{ csrf_token() }}', id:ell.value, status:status}, function(data){
                if(data == 1){
                    alert('{{trans('member.successchangestatus')}}');
                }
                else{
                    alert('{{trans('member.successerrorchangestatus')}}');
                }
            });
        }

    </script>
@endsection
