@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('user_subscription.USER SUBSCRIPTION DETAILS')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html" data-original-title="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
                                <li class="breadcrumb-item active">{{trans('user_subscription.USER SUBSCRIPTION DETAILS')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('user_subscription.USER SUBSCRIPTION DETAILS')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('subcategory.price')}}</th>
                                        <th>{{trans('reservation.date')}}</th>
                                        <th>{{trans('reservation.time')}}</th>
                                        <th>{{trans('reservation.ordernumber')}}</th>
                                        <th>{{trans('reservation.createdat')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <td>{{$userSubscription->id}}</td>
                                    <td>{{$userSubscription->total_price}}</td>
                                    <td>{{$userSubscription->date}}</td>
                                    <td>{{$userSubscription->time}}</td>
                                    <td>{{$userSubscription->subscription_number}}</td>
                                    <td>{{$userSubscription->created_at}}</td>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('user.userDetails')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>
                                        <th>{{trans('dashboard.username')}}</th>
                                        <th>{{trans('user.email')}}</th>
                                        <th>{{trans('user.image')}}</th>
                                        <th>{{trans('user.phone')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>


                                    <td> <a href="{{route('users.show',$userSubscription->user->id)}}" data-original-title="" title="">{{ $userSubscription->user->name}}</a></td>
                                    <td>{{ $userSubscription->user->email}}</td>
                                    <td><img class="img-fluid img-60" src="{{ $userSubscription->user->image}}" alt="#" data-original-title="" title=""></td>
                                    <td>{{ $userSubscription->user->phone}}</td>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('user_subscription.TECHNICIAN DATA')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>
                                        <th>{{trans('dashboard.username')}}</th>
                                        <th>{{trans('user.email')}}</th>
                                        <th>{{trans('user.image')}}</th>
                                        <th>{{trans('user.phone')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($userSubscription->technician($userSubscription->id)))
                                    <td> <a href="{{route('users.show',$userSubscription->technician($userSubscription->id)) }}" data-original-title="" title="">{{ $userSubscription->technician($userSubscription->id)->name }}</a></td>
                                    <td>{{ $userSubscription->technician($userSubscription->id)->email }}</td>
                                    <td><img class="img-fluid img-60" src="{{ $userSubscription->technician($userSubscription->id)->image }}" alt="#" data-original-title="" title=""></td>
                                    <td>{{ $userSubscription->technician($userSubscription->id)->phone }}</td>
                                    @else
                                        <td></td>
                                        <td class="all-msgs text-center">
                                        <h5>{{trans('user_subscription.no technician assigned yet')}}</h5>
                                        </td>
                                        <td></td>
                                    @endif


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('user_subscription.CATEGORY AND PACKAGE DATA')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>
                                        <th>{{trans('user_subscription.category')}}</th>
                                        <th>{{trans('user_subscription.package')}}</th>

                                    </tr>
                                    </thead>
                                    <tbody>


                                    <td>{{ $userSubscription->subscription->category->name_en}}</td>
                                    <td>{{ $userSubscription->subscription->package->name_en}}</td>



                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection
