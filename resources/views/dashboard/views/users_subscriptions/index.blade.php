@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('user_subscription.USERS SUBSCRIPTIONS TABLE')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">{{trans('user_subscription.USERS SUBSCRIPTIONS TABLE')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    @include('dashboard.layouts.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('user_subscription.USERS SUBSCRIPTIONS TABLE')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('user.name')}}</th>
                                        <th>{{trans('admin.email')}}</th>
                                        <th>{{trans('user_subscription.subscription')}}</th>
                                        <th>{{trans('user_subscription.price')}}</th>
                                        <th>{{trans('admin.status')}}</th>
                                        <th>{{trans('admin.created')}}</th>
                                        <th>{{trans('admin.actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($subscriptions as $key=>$subscription)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{$subscription->user->name}}</td>
                                            <td>{{$subscription->user->email}}</td>
                                            <td>{{$subscription->subscription->package->name_en}}</td>
                                            <td>{{$subscription->total_price}} {{$subscription->currency}}</td>
                                            <td>
                                                <div class="media-body text-right icon-state">
                                                    <label class="switch">
                                                        <input onchange="updateStatus(this)" value="{{ $subscription->id }}" type="checkbox"
                                                        <?php if($subscription->accepted == 1) echo "checked";?>>
                                                        <span class="switch-state bg-primary"></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>{{$subscription->created_at}}</td>
                                            <td>
                                                <a href="{{ route('users.subscriptions.show',$subscription->id) }}" class="btn btn-info">{{trans('reservation.details')}}</a>

                                                <form id="delete-form-{{ $subscription->id }}" action="{{ route('users.subscriptions.destroy',$subscription->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger" onclick="if(confirm('are you sure ? you want to delete this field ?')){
                                                    event.preventDefault();
                                                    document.getElementById('delete-form-{{ $subscription->id }}').submit();
                                                    }else {
                                                    event.preventDefault();
                                                    }">{{trans('reservation.delete')}}</button>
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection
@section('myjsfile')
    <script>
        function updateStatus(elUser){
            if(elUser.checked){
                var accepted = 1;
            }
            else{
                var accepted = 0;
            }
            $.post('{{ route('users.subscriptions.status',isset($subscription) ? $subscription->id : "") }}', {_token:'{{ csrf_token() }}', id:elUser.value, accepted:accepted}, function(data){
                if(data == 1){
                    alert('subscription status changed successfully');
                }
                else{
                    alert('something went error');
                }
            });
        }
    </script>
@endsection
