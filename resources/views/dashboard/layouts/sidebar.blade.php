<div class="page-sidebar">
    @if(request()->segment(1)=='en')
    <div class="main-header-left d-none d-lg-block">
        <div class="logo-wrapper"><a href="{{route('dashboard')}}"><img src="{{ asset('assets/dashboard/images/logo.png') }}" style="margin-left: 50px" width="100px" alt=""></a></div>
    </div>
    @else
        <div class="main-header-right d-none d-lg-block">
            <div class="logo-wrapper"><a href="{{route('dashboard')}}"><img src="{{ asset('assets/dashboard/images/logo.png') }}" style="margin-left: 80px" width="100px" alt=""></a></div>
        </div>
    @endif
    <div class="sidebar custom-scrollbar">
        <div class="sidebar-user text-center">
            <div><img class="img-60 rounded-circle" src="{{ asset(Auth::user()->image) }}" alt="#">
                <div class="profile-edit"><a href="{{route('profile')}}" target="_blank"><i data-feather="edit"></i></a></div>
            </div>
            <h6 class="mt-3 f-14">{{Auth::user()->name}}</h6>
            <p>{{Auth::user()->email}}</p>
        </div>
        <ul class="sidebar-menu">
            <li><a class="sidebar-header" href="{{route('dashboard')}}"><i data-feather="home"></i>
                    <span>{{trans('dashboard.dashboard')}}</span>
                  </a>
            </li>
            @if(Auth::user()->user_type == 'admin' || in_array('1', json_decode(Auth::user()->staff->role->permissions)))
            <li><a class="sidebar-header" href="{{route('users.index')}}"><i data-feather="users"></i><span>{{trans('dashboard.users')}}</span></a>
            </li>
            @endif
            @if(Auth::user()->user_type == 'admin' || in_array('2', json_decode(Auth::user()->staff->role->permissions)))
            <li><a class="sidebar-header" href="{{route('admins.index')}}"><i data-feather="users"></i><span>{{trans('dashboard.admins')}}</span></a>
            </li>
            @endif

{{--            @if(Auth::user()->user_type == 'admin' || in_array('3', json_decode(Auth::user()->staff->role->permissions)))--}}
{{--            <li><a class="sidebar-header" href="{{route('members.index')}}"><i data-feather="users"></i><span>{{trans('dashboard.members')}}</span></a>--}}
{{--            </li>--}}
{{--            @endif--}}

            @if(Auth::user()->user_type == 'admin' || in_array('4', json_decode(Auth::user()->staff->role->permissions)))
            <li><a class="sidebar-header" href="{{route('technicians.index')}}"><i data-feather="users"></i><span>{{trans('dashboard.technicians')}}</span></a>
            </li>
            @endif
            @if(Auth::user()->user_type == 'admin' || in_array('5', json_decode(Auth::user()->staff->role->permissions)))
            <li><a class="sidebar-header" href="{{route('cities.index')}}"><i data-feather="map-pin"></i><span>{{trans('dashboard.cities')}}</span></a>
            </li>
            @endif
            @if(Auth::user()->user_type == 'admin' || in_array('6', json_decode(Auth::user()->staff->role->permissions)))
            <li><a class="sidebar-header" href="{{route('categories.index')}}"><i data-feather="layers"></i><span>{{trans('dashboard.categories')}}</span></a>
            </li>
            @endif
            @if(Auth::user()->user_type == 'admin' || in_array('7', json_decode(Auth::user()->staff->role->permissions)))
            <li><a class="sidebar-header" href="{{route('units.index')}}"><i data-feather="list"></i><span>{{trans('dashboard.units')}}</span></a>
            </li>
            @endif
            @if(Auth::user()->user_type == 'admin' || in_array('8', json_decode(Auth::user()->staff->role->permissions)))
            <li><a class="sidebar-header" href="{{route('subcategories.index')}}"><i data-feather="list"></i><span>{{trans('dashboard.subcategories')}}</span></a>
            </li>
            @endif
            @if(Auth::user()->user_type == 'admin' || in_array('9', json_decode(Auth::user()->staff->role->permissions)))
            <li><a class="sidebar-header" href="{{route('packages.index')}}"><i data-feather="box"></i><span>{{trans('dashboard.packages')}}</span></a>
            </li>
            @endif
            @if(Auth::user()->user_type == 'admin' || in_array('10', json_decode(Auth::user()->staff->role->permissions)))
            <li><a class="sidebar-header" href="{{route('subscriptions.index')}}"><i data-feather="box"></i><span>{{trans('dashboard.subscriptions')}}</span></a>
            </li>
            @endif
            @if(Auth::user()->user_type == 'admin' || in_array('11', json_decode(Auth::user()->staff->role->permissions)))
            <li><a class="sidebar-header" href="{{route('users.subscriptions.index')}}"><i data-feather="share-2"></i><span>{{trans('dashboard.Users subscriptions')}}</span></a>
            </li>
            @endif
            @if(Auth::user()->user_type == 'admin' || in_array('12', json_decode(Auth::user()->staff->role->permissions)))
            <li><a class="sidebar-header"><i data-feather="star"></i><span>{{trans('dashboard.ratings')}}</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="{{route('users.rates.index')}}"><i class="fa fa-circle"></i>{{trans('dashboard.users')}}</a></li>
                    <li><a href="{{route('technicians.rates.index')}}"><i class="fa fa-circle"></i>{{trans('dashboard.technicians')}}</a></li>
                </ul>
            </li>
            @endif

            @if(Auth::user()->user_type == 'admin' || in_array('13', json_decode(Auth::user()->staff->role->permissions)))
            <li><a class="sidebar-header"><i data-feather="tag"></i><span>{{trans('dashboard.orders')}}</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="{{route('orders.new.index')}}"><i class="fa fa-circle"></i>new orders</a></li>
                    <li><a href="{{route('orders.inprogress.index')}}"><i class="fa fa-circle"></i>in progress</a></li>
                    <li><a href="{{route('orders.finished.index')}}"><i class="fa fa-circle"></i>finished orders</a></li>
                    <li><a href="{{route('orders.unaccepted.index')}}"><i class="fa fa-circle"></i>unaccepted orders</a></li>

                </ul>
            </li>
            @endif

            @if(Auth::user()->user_type == 'admin' || in_array('14', json_decode(Auth::user()->staff->role->permissions)))
            <li><a class="sidebar-header"><i data-feather="clipboard"></i><span>{{trans('permissions.permissions')}}</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="{{ route('staffs.index') }}"><i class="fa fa-circle"></i>{{trans('dashboard.All Staffs')}}</a></li>
                    <li><a href="{{route('roles.index')}}"><i class="fa fa-circle"></i>{{trans('dashboard.Staff permissions')}}</a></li>
                </ul>
            </li>
            @endif

            @if(Auth::user()->user_type == 'admin' || in_array('15', json_decode(Auth::user()->staff->role->permissions)))
            <li><a class="sidebar-header" href="{{route('suggestions.index')}}"><i data-feather="mail"></i><span>{{trans('dashboard.suggestions')}}</span></a>
            </li>
            @endif
            @if(Auth::user()->user_type == 'admin' || in_array('16', json_decode(Auth::user()->staff->role->permissions)))
            <li><a class="sidebar-header" href="{{route('settings')}}"><i data-feather="settings"></i><span>{{trans('dashboard.settings')}}</span></a>
            </li>
            @endif

            @if(Auth::user()->user_type == 'admin' || in_array('17', json_decode(Auth::user()->staff->role->permissions)))
            <li><a class="sidebar-header" href="{{route('notifications.index')}}"><i data-feather="bell"></i><span>{{trans('dashboard.notifications')}}</span></a>
            </li>
            @endif
        </ul>
    </div>
</div>
