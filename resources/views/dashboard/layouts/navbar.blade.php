<div class="page-main-header">
    <div class="main-header-right row">
        <div class="main-header-left d-lg-none">
            <div class="logo-wrapper"><a href="index.html"><img src="{{ asset('assets/dashboard/images/endless-logo.png') }}" alt=""></a></div>
        </div>
        <div class="mobile-sidebar d-block">
            <div class="media-body text-right switch-sm">
                <label class="switch"><a href="#"><i id="sidebar-toggle" data-feather="align-left"></i></a></label>
            </div>
        </div>
        <div class="nav-right col p-0">
            <ul class="nav-menus">
                <li>

                </li>

                <li class="onhover-dropdown"><a class="txt-dark" href="index.html#">
                        <h6>
                            @if(request()->segment(1)=='en')
                                EN
                            @else
                                AR
                            @endif
                        </h6></a>
                    <ul class="language-dropdown onhover-show-div p-20">

                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <li><a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}" data-lng="en"><i class="flag-icon"></i> {{ $properties['native'] }}</a></li>
                        @endforeach

                    </ul>
                </li>
                <li class="onhover-dropdown"><i data-feather="mail"></i><span class="dot"></span>
                    <ul class="notification-dropdown onhover-show-div">
                        <li>{{trans('dashboard.notifications')}}<span class="badge badge-pill badge-primary pull-right">{{countUnreadMsg()}}</span></li>
                        @if(countUnreadMsg()>0)
                            @foreach(unreadMsg() as $keyMessages => $valueMessage)
                                <li>
                                    <a href="{{route('suggestions.show',$valueMessage->id)}}">
                                        <div class="media">
                                            <div class="media-body">
                                                <h6 class="mt-0"><span><i class="shopping-color" data-feather="mail"></i></span>{{$valueMessage->user->name}}<small class="pull-right">{{$valueMessage->created_at}}</small></h6>
                                                <p class="mb-0">{{str_limit($valueMessage->suggestion,10)}}.</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endforeach
                                <a href="{{route('suggestions.index')}}"><li class="bg-light txt-dark">{{trans('dashboard.seeAllMessages')}}</li></a>
                        @else
                            <li class="all-msgs text-center">
                                <p class="m-0"><a href="#">{{trans('dashboard.nomessagesfound')}}</a></p>
                            </li>
                        @endif
                    </ul>
                </li>



                <li class="onhover-dropdown"><i data-feather="tag"></i><span class="dot"></span>
                    <ul class="notification-dropdown onhover-show-div">
                        <li>{{trans('reservation.orders')}} <span class="badge badge-pill badge-primary pull-right">{{countUnActiveOrder()}}</span></li>
                        @if(countUnActiveOrder()>0)
                            @foreach(unActiveOrder() as $keyMessages => $valueMessage)
                        <li>
                            <a href="{{route('orders.show',$valueMessage->id)}}">
                            <div class="media">
                                <div class="media-body">
                                    <h6 class="mt-0"><span><i class="shopping-color" data-feather="shopping-bag"></i></span>{{$valueMessage->user->name}}<small class="pull-right">{{$valueMessage->time}}</small></h6>
                                    <p class="mb-0">{{$valueMessage->street}}</p>
                                </div>
                            </div>
                            </a>
                        </li>
                            @endforeach
                        <li class="bg-light txt-dark"><a href="{{route('orders.new.index')}}"></a> {{trans('reservation.allorders')}}</li>
                        @else
                            <li class="all-msgs text-center">
                                <p class="m-0"><a href="#">{{trans('reservation.no orders found')}}</a></p>
                            </li>
                        @endif
                    </ul>
                </li>



                <li class="onhover-dropdown">
                    <div class="media align-items-center"><img class="align-self-center pull-right img-50 rounded-circle" src="{{ asset(Auth::user()->image) }}" alt="header-user">
                        <div class="dotted-animation"><span class="animate-circle"></span><span class="main-circle"></span></div>
                    </div>
                    @if(request()->segment(1)=='en')
                    <ul class="profile-dropdown onhover-show-div p-20">
                        @else
                            <ul class="profile-dropdown onhover-show-div p-20" style="margin-right: -35px">
                                @endif
                        <li><a href="{{route('profile')}}"><i data-feather="user"></i>                                   {{trans('profile.EditAdminProfile')}}</a></li>
                        <li><a href="{{route('orders.new.index')}}"><i data-feather="tag"></i>                                  {{trans('reservation.orders')}}</a></li>
                        <li><a href="{{route('backup')}}"><i data-feather="database"></i> {{trans('dashboard.backup')}}</a></li>
                        <li><a href="{{route('settings')}}"><i data-feather="settings"></i>                                     {{trans('dashboard.settings')}}</a></li>
                        <li><a href="{{route('logout')}}"  onclick="event.preventDefault(); document.getElementById('frm-logout').submit();"><i data-feather="log-out"></i>
                                {{trans('dashboard.logout')}}</a>
                            <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
            <div class="d-lg-none mobile-toggle pull-right"><i data-feather="more-horizontal"></i></div>
        </div>
        <script id="result-template" type="text/x-handlebars-template">
            <div class="ProfileCard u-cf">
                <div class="ProfileCard-avatar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-airplay m-0"><path d="M5 17H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-1"></path><polygon points="12 15 17 21 7 21 12 15"></polygon></svg></div>
                <div class="ProfileCard-details">
                    <div class="ProfileCard-realName">name</div>
                </div>
            </div>
        </script>
        <script id="empty-template" type="text/x-handlebars-template">
            <div class="EmptyMessage">Your search turned up 0 results. This most likely means the backend is down, yikes!</div>

        </script>
    </div>
</div>
