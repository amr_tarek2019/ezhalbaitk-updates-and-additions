<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderRate extends Model
{
    protected $table='orders_rate';
    protected $fillable=['user_id', 'technician_id', 'order_id', 'rate'];

    public function order()
    {
        return $this->belongsTo('App\Order','order_id');
    }

    public function technician()
    {
        return $this->belongsTo('App\Technician','technician_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
