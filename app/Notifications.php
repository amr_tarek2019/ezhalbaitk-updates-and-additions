<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    protected $table='notifications';
    protected $fillable=['user_id', 'order_id', 'text_en','text_ar'];

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toDateString();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
