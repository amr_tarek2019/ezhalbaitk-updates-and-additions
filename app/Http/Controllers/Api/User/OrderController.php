<?php

namespace App\Http\Controllers\Api\User;

use App\Cart;
use App\Category;
use App\Http\Controllers\Api\BaseController;
use App\JobDetails;
use App\Order;
use App\OrderImage;
use App\OrderRequest;
use App\OrderSubcategory;
use App\PushNotification;
use App\Subcategory;
use App\Technician;
use App\TechnicianOrder;
use App\User;
use App\RateTechnician;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Notifications;
use App\UserSubscription;
use App\Subscription;


class OrderController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createOrder(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token',$jwt)->first();
        $token = \App\User::where('firebase_token',$user->id)->pluck('firebase_token')->toArray();

        if (!$user) {

            return $response=[
                'success'=>403,
                'message'=>trans('api.please login first'),
            ];
        }
        $validator = Validator::make($request->all(), [
            'payment_type' => 'required',
            'city_id'=>'required',
            'date' => 'required',
            'time' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'area_num'=>'required',
            'area' => 'required',
            'block' => 'required',
            'street' => 'required',
            'house' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $order_number = rand(111111111, 999999999);

        $order= Order::create(array_merge($request->all(),[
            'user_id'=>$user->id,
            'order_number'=>$order_number,
            'subscription_id'=>$request->subscription_id,
            'floor'=>$request->floor,
            'appartment'=>$request->appartment,
            'directions'=>$request->directions,
        ]));
        $order->save();

        $validator = Validator::make($request->all(), [
            'subcategory_id' => 'required',
            'quantity'=>'required'
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $price = Subcategory::where('id',$request->subcategory_id)->select('price')->first();
        for($i=0;$i<count($request->subcategory_id);$i++)
        {
            $quantity=$request->quantity[$i];
            $total=$quantity*$price['price'];
            $orders=new OrderSubcategory();
            $orders->order_id=$order->id;
            $orders->subcategory_id=$request->subcategory_id[$i];
            $orders->total=$total;
            $orders->quantity=$quantity;
            $orders->save();
        }

        if($request->technician_id)
        {
            $newJob=new JobDetails();
            $newJob->order_id=$order->id;
            $newJob->technician_id=$request->technician_id;
            $newJob->user_id=$user->id;
            $newJob->save();

            $newJobToTech=new TechnicianOrder();
            $newJobToTech->order_id=$order->id;
            $newJobToTech->technician_id=$request->technician_id;
            $newJobToTech->order_number=$order->order_number;
            $newJobToTech->save();
        }
        if($request->subscription_id)
        {
            $subscriptionPrice = UserSubscription::where('subscription_id',$request->subscription_id)->select('total_price')->first();
            $residual = $subscriptionPrice->total_price -  $orders->total;

            $getSubcategory=UserSubscription::where('subscription_id',$request->subscription_id)->select('id')->first();
            $getSubcategory->total_price=$residual;

            $getSubcategory->save();
        }


        $notifications=new Notifications();
        $notifications->user_id=$user->id;
        $notifications->order_id=$order->id;
        $notifications->text_en='job successfully posted';
        $notifications->text_ar='تم نشر الوظيفة بنجاح';
        $notifications->save();


        $subscriptions = UserSubscription::where('user_id', $user->id)->get();



        $res_item = [];
        $res_list  = [];
        foreach ($subscriptions as $res) {
            $res_item['id'] = $res->id;
            $cat=Subscription::where('id',$res->subscription_id)->pluck('category_id')->first();

            $category=Category::where('id',$cat)->select('id','name_'.$lang.' as name')->first();

            $res_item['category_id'] = $category->id;
            $res_item['id'] = $res->subscription_id;
            $res_item['price'] = $res->total_price;
            $res_item['currency'] = $res->currency;
            $res_list[] = $res_item;
        }



        PushNotification::send($token,trans('api.job successfully posted'),1);

        if (isset($request->technician_id))
        {
            $tokens=\App\User::where('user_type','technicians')->pluck('firebase_token')->toArray();
            PushNotification::send_details($tokens, $order->status,$order->order_number,trans('api.job successfully posted'),$order->id,1);
        }


        $response=[
            'status'=>200,
            'message'=>trans('api.reservation'),
            'data'=>isset($request->subscription_id) ? $res_list : [],
        ];
        return \Response::json($response,200);



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function orderStatus(Request $request)
    {
        $order=Order::where('id',$request->order_id)->select('status')->first();
        $data['job_status'] = $order['status'];
        if ($order)
        {
            $response=[
                'message'=>'get status of job successfully',
                'status'=>200,
                'data'=>$data,
            ];
        }else{
            $response=[
                'message'=>'something went wrong',
                'status'=>404,
            ];
        }
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function JobDetails(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jobDetails=JobDetails::where('order_id',$request->order_id)->get();
        $order_id=JobDetails::where('order_id',$request->order_id)->exists();
        if($order_id) {
            if (!$jobDetails)
            {
                return  $response = [
                    'message' =>'no data found',
                    'status' => 404,
                ];

            }else{
                $res_item = [];
                $res_list  = [];
                foreach ($jobDetails as $res) {
                    $res_item['id'] = $res->id;
                    $res_item['order_id'] = $res->order_id;
                    $technicianId=JobDetails::where('order_id',$request->order_id)->pluck('technician_id')->first();

                    $technician_id=Technician::where('id',$technicianId)->pluck('user_id')->first();

                    $technician=User::where('id',$technician_id)->where('user_type','technician')->select('lat','lng','name','phone','image','address')->first();
                    if (isset($technician))
                    {
                        $res_item['technician']=$technician;
                    }else{
                        $res_item['technician']=null;
                    }

                    $subcategoryData=JobDetails::where('order_id',$request->order_id)->pluck('order_id')->first();
                    $subcategoryId=OrderSubcategory::where('order_id',$subcategoryData)->pluck('subcategory_id')->first();



                    $subcategory = Subcategory::where('id',$subcategoryId)->select('name_'.$lang. ' as name','price')->get();




                    $res_item['subcategory']=$subcategory;

                    $orderId=JobDetails::where('order_id',$request->order_id)->pluck('order_id')->first();
                    $orderData=Order::where('id',$orderId)->select('area_num','area','block','street','house')->first();


                    $res_item['order_details'] = $orderData;


                    $job=JobDetails::where('order_id',$request->order_id)->pluck('order_id')->first();
                    $order=Order::where('id',$job)->select('payment_type','currency')->first();
                    $res_item['payment_type'] = $order->payment_type;
                    $res_item['currency'] = $order->currency;

                    $dateAndTime=JobDetails::where('order_id',$request->order_id)->pluck('order_id')->first();
                    $orderDateAndTime=Order::where('id',$job)->select('date','time')->first();
                    $res_item['date'] = $orderDateAndTime->date;
                    $res_item['time'] = $orderDateAndTime->time;

                    $techId=JobDetails::where('order_id',$request->order_id)->pluck('technician_id')->first();



                    $techRate=RateTechnician::where('technician_id',$techId)->select('rate')->avg('rate');
                    $res_item['technicianRate']=(string)$techRate;


                    $totalPrice=JobDetails::where('order_id',$request->order_id)->pluck('order_id')->first();
                    $total=OrderSubcategory::where('order_id',$totalPrice)->select('total')->first();
                    $res_item['total']=$total->total;

                    $orderSubscription=Order::where('id',$request->order_id)->pluck('subscription_id')->first();

                    if (!empty($orderSubscription))
                    {
                        $subscriptionData=UserSubscription::where('subscription_id',$orderSubscription)->select('total_price',
                            'date','time','subscription_number')->first();
                        $res_item['subscription_Data']=$subscriptionData;
                    }else{
                        $res_item['subscription_Data']='there is no user subscription';
                    }

                    $res_list = $res_item;

                }

                $response = [
                    'message' =>'get data of job successfully',
                    'status' => 200,
                    'data' =>  $res_list,
                ];
            }
            return \Response::json($response, 200);
            if (!$request->headers->has('jwt')) {
                return response(401, 'check_jwt');
            } elseif (!$request->headers->has('lang')) {
                return response(401, 'check_lang');
            }

        }else{
            return  $response = [
                'message' =>'no technician assigned yet',
                'status' => 401,
            ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'date'=>'required',
            'time'=>'required',
        ]);
        if ($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $reschedule=Order::find($request->id);
        $reschedule->date = $request->date;
        $reschedule->time = $request->time;
        $reschedule->save();
        $response=[
            'message'=>'request updated successfully',
            'status'=>200,
        ];
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $order =Order::find($request->id);
        if (\App\Order::destroy($request->id)){

            $response=[
                'message'=>trans('api.Order successfully deleted'),
                'status'=>200,
            ];
            return \Response::json($response,200);
        }else{
            $response=[
                'message'=>trans('api.somethingwentwrong'),
                'status'=>401,
            ];
            return \Response::json($response,404);
        }
    }
}
