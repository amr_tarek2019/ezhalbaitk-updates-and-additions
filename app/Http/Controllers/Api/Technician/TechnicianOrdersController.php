<?php

namespace App\Http\Controllers\Api\Technician;

use App\Category;
use App\Http\Controllers\Api\BaseController;
use App\JobDetails;
use App\Order;
use App\OrderSubcategory;
use App\Subcategory;
use App\TechnicalReport;
use App\Technician;
use App\TechnicianOrder;
use App\Unit;
use App\User;
use App\UserSubscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\PushNotification;
use App\OrderRate;
use App\Notifications;


class TechnicianOrdersController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function nextVisit(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user=User::where('jwt_token',$jwt)->where('user_type','technician')->first();
        $technician=Technician::where('user_id',$user->id)->pluck('id')->first();

        $nextVisitOrders=TechnicianOrder::where('technician_id',$technician)->where('status','0')->get();
        $res_item = [];
        $res_list  = [];
        foreach ($nextVisitOrders as $res) {
            $res_item['id'] = $res->id;
            $res_item['order_number'] = $res->order_number;
            $res_item['status'] = $res->status;
            $getRequestDateTime=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();

            $timeAndDate=Order::where('id',$getRequestDateTime)->select('time','date','id')->first();

            $res_item['time'] = $timeAndDate->time;
            $res_item['date'] = $timeAndDate->date;
            $res_item['order_id'] = $timeAndDate->id;

            $techOrderId=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $subcategoryId=OrderSubcategory::where('order_id',$techOrderId)->pluck('subcategory_id')->first();
            $unitId=Subcategory::where('id',$subcategoryId)->pluck('unit_id')->first();
            $categoryId=Unit::where('id',$unitId)->pluck('category_id')->first();
            $getCategoryName=Category::where('id',$categoryId)->select('name_'.$lang.' as name')->first();
            $res_item['category_name'] = $getCategoryName->name;


            $getUserName=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $userName=Order::where('id',$getUserName)->pluck('user_id')->first();
            $user=User::where('id',$userName)->select('name')->first();
            $res_item['user_name'] = $user->name;


            $res_list[] = $res_item;
        }

        $response = [
            'message' => 'get data of available orders successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function inProgress(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user=User::where('jwt_token',$jwt)->where('user_type','technician')->first();
        $technician=Technician::where('user_id',$user->id)->pluck('id')->first();


        $nextVisitOrders=TechnicianOrder::where('technician_id',$technician)->where('status','1')
            ->orWhere('status','2')->orWhere('status','3')
            ->orWhere('status','4')
            ->get();
        $res_item = [];
        $res_list  = [];
        foreach ($nextVisitOrders as $res) {
            $res_item['id'] = $res->id;
            $res_item['order_number'] = $res->order_number;
            $res_item['status'] = $res->status;
            $getRequestDateTime=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $timeAndDate=Order::where('id',$getRequestDateTime)->select('time','date','id')->first();

            $res_item['time'] = $timeAndDate->time;
            $res_item['date'] = $timeAndDate->date;
            $res_item['order_id'] = $timeAndDate->id;

            $techOrderId=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();

            $subcategoryId=OrderSubcategory::where('order_id',$techOrderId)->pluck('subcategory_id')->first();

            $unitId=Subcategory::where('id',$subcategoryId)->pluck('unit_id')->first();

            $categoryId=Unit::where('id',$unitId)->pluck('category_id')->first();

            $getCategoryName=Category::where('id',$categoryId)->select('name_'.$lang.' as name')->first();

            $res_item['category_name'] = $getCategoryName->name;


            $getUserName=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $userName=Order::where('id',$getUserName)->pluck('user_id')->first();
            $user=User::where('id',$userName)->select('name')->first();
            $res_item['user_name'] = $user->name;


            $res_list[] = $res_item;
        }

        $response = [
            'message' => 'get data of available orders successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function completed(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user=User::where('jwt_token',$jwt)->where('user_type','technician')->first();
        $technician=Technician::where('user_id',$user->id)->pluck('id')->first();

        $nextVisitOrders=TechnicianOrder::where('technician_id',$technician)->where('status','5')->get();
        $res_item = [];
        $res_list  = [];
        foreach ($nextVisitOrders as $res) {
            $res_item['id'] = $res->id;
            $res_item['order_number'] = $res->order_number;
            $res_item['status'] = $res->status;
            $getRequestDateTime=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $timeAndDate=Order::where('id',$getRequestDateTime)->select('time','date','id')->first();
            $res_item['time'] = $timeAndDate->time;
            $res_item['date'] = $timeAndDate->date;
            $res_item['order_id'] = $timeAndDate->id;

            $techOrderId=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $subcategoryId=OrderSubcategory::where('order_id',$techOrderId)->pluck('subcategory_id')->first();
            $unitId=Subcategory::where('id',$subcategoryId)->pluck('unit_id')->first();
            $categoryId=Unit::where('id',$unitId)->pluck('category_id')->first();
            $getCategoryName=Category::where('id',$categoryId)->select('name_'.$lang.' as name')->first();
            $res_item['category_name'] = $getCategoryName->name;


            $getUserName=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $userName=Order::where('id',$getUserName)->pluck('user_id')->first();
            $user=User::where('id',$userName)->select('name')->first();
            $res_item['user_name'] = $user->name;


            $res_list[] = $res_item;
        }

        $response = [
            'message' => 'get data of available orders successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function orderDetails(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';

        $orderDetails= TechnicianOrder::where('order_id',$request->id)->get();
        $res_item = [];
        $res_list  = [];
        foreach ($orderDetails as $res) {
            // $res_item['id'] = $res->id;
            $res_item['order_number'] = $res->order_number;
            $res_item['status'] = $res->status;
            $getRequestDateTime=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $timeAndDate=Order::where('id',$getRequestDateTime)->select('time','date','note','image')->first();
            $res_item['time'] = $timeAndDate->time;
            $res_item['date'] = $timeAndDate->date;

            if(!empty($timeAndDate->note)) {
                $res_item['note'] = $timeAndDate->note;
            }else{
                $res_item['note']='';
            }
            if(!empty($timeAndDate->image)) {
                $res_item['note_image'] = $timeAndDate->image;
            }else{
                $res_item['note_image']='';
            }

            $techOrderId=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $subcategoryId=OrderSubcategory::where('order_id',$techOrderId)->pluck('subcategory_id')->first();
            $unitId=Subcategory::where('id',$subcategoryId)->pluck('unit_id')->first();
            $categoryId=Unit::where('id',$unitId)->pluck('category_id')->first();
            $getCategoryName=Category::where('id',$categoryId)->select('name_'.$lang.' as name')->first();



            $getUserName=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $userName=Order::where('id',$getUserName)->pluck('user_id')->first();
            $user=User::where('id',$userName)->select('lat','lng')->first();
            $res_item['user_lat'] = $user->lat;
            $res_item['user_lng'] = $user->lng;


            $subId=OrderSubcategory::where('order_id',$res->order_id)->pluck('subcategory_id');
            $subName=Subcategory::whereIn('id',$subId)->select('name_'.$lang. ' as name')->get();
            $quantity=OrderSubcategory::where('order_id',$res->order_id)->select('quantity')->get();
            $res_item['issue']=$subName;


            $res_item['quantity']=$quantity;


//            $subcategoryData=JobDetails::where('order_id',$res->order_id)->pluck('order_id')->first();
//            $subcategoryId=OrderSubcategory::where('order_id',$subcategoryData)->pluck('subcategory_id')->first();
//
//
//
//            $subcategory = Subcategory::where('id',$subcategoryId)->select('name_'.$lang. ' as name')->get();
//
//
//
//
//            $res_item['subcategory']=$subcategory;

            $getOrderDetails=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $order=Order::where('id',$getOrderDetails)->select('street','area_num','area','block','house','payment_type','currency','id')->first();
            $res_item['street']=$order->street;
            $res_item['area_num']=$order->area_num;
            $res_item['area']=$order->area;
            $res_item['block']=$order->block;
            $res_item['house']=$order->house;
            $res_item['payment_type']=$order->payment_type;
            $res_item['currency']=$order->currency;
            $res_item['id']=$order->id;


            $getSumOfOrder=OrderSubcategory::where('order_id',$res->order_id)->select('total')->sum('total');
            $res_item['total']=(string)$getSumOfOrder;


            $orderRate=OrderRate::where('order_id',$request->id)->exists();

            if(!empty($orderRate)) {
                $res_item['rate']=true;
            } else {
                $res_item['rate']=false;
            }

            $res_list = $res_item;
        }
        $response = [
            'message' => 'get data of order details successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function acceptOrderForm(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';

        $orderDetails= TechnicianOrder::where('order_id',$request->id)->get();
        $res_item = [];
        $res_list  = [];
        foreach ($orderDetails as $res) {
            // $res_item['id'] = $res->id;
            $res_item['order_number'] = $res->order_number;
            $res_item['status'] = $res->status;
            $getUserName=TechnicianOrder::where('order_id',$res->order_id)->pluck('order_id')->first();
            $userName=Order::where('id',$getUserName)->pluck('user_id')->first();
            $user=User::where('id',$userName)->select('lat','lng','address','phone','name')->first();
            $res_item['user_lat'] = $user->lat;
            $res_item['user_lng'] = $user->lng;
            $res_item['user_name'] = $user->name;
            $res_item['user_address'] = $user->address;
            $res_item['user_phone']= ltrim($user['phone'], '+‎974');
            $technician=Technician::where('id',$res->technician_id)->pluck('user_id')->first();
            $userTechnician=User::where('id',$technician)->select('lat','lng','address')->first();

            $res_item['technician_lat'] = $userTechnician->lat;
            $res_item['technician_lng'] = $userTechnician->lng;
            $res_item['technician_address'] = $userTechnician->address;

            $getSumOfOrder=OrderSubcategory::where('order_id',$res->order_id)->select('total')->sum('total');
            $res_item['total']=(string)$getSumOfOrder;

            $subcategoryData=JobDetails::where('order_id',$res->order_id)->pluck('order_id')->first();
            $subcategoryId=OrderSubcategory::where('order_id',$subcategoryData)->pluck('subcategory_id')->toArray();




            $subcategory = Subcategory::where('id',$subcategoryId)->select('name_'.$lang. ' as name')->get();




            $res_item['subcategory']=$subcategory;


            $getOrderNotesAndImage=Order::where('id',$getUserName)->select('note','image','currency','id')->first();
            $res_item['order_image'] = $getOrderNotesAndImage->image;
            $res_item['order_notes'] = $getOrderNotesAndImage->note;
            $res_item['currency'] = $getOrderNotesAndImage->currency;
            $res_item['id'] = $getOrderNotesAndImage->id;

            $unit='K';
            $theta = $userTechnician->lng - $user->lng;
            $dist = sin(deg2rad( $userTechnician->lat)) * sin(deg2rad($user->lat)) +  cos(deg2rad($userTechnician->lat)) * cos(deg2rad($user->lat)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles =(string) round($dist * 60 * 1.1515);
            $unit = strtoupper($unit);
            if ($unit == "K")
            {
                $res_item['distance'] = (string)round($miles * 1.609344,2). " KM";
            }
            else if ($unit == "N")
            {
                $res_item['distance'] =(string) round($miles * 0.8684,2);
            }
            else {
                $res_item['distance'] =(string) round($miles,2);
            }


            $res_list = $res_item;
        }
        $response = [
            'message' => 'get data of order details successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function replyOnOrder(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->pluck('id')->first();
        $technician=Technician::where('user_id',$user)->select('id')->first();
        if (!$user) {

            return $response=[
                'success'=>403,
                'message'=>trans('api.please login first'),
            ];
        }
        $validator = Validator::make($request->all(), [
            'accepted'=>'required',
        ]);
        if ($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $orderReply=TechnicianOrder::where('order_id',$request->id)->first();
        $orderReply->accepted = $request->accepted;
        if ($request->accepted=='0')
        {
            $orderReply->status = '6';
            $orderReply->technician_id = null;
        }elseif ($request->accepted=='1')
        {
            $orderReply->status = '1';
        }
        $orderReply->save();

        $getOrderId=TechnicianOrder::where('order_id',$request->id)->pluck('order_id')->first();

        $changeOrderStatusForUser=Order::where('id',$getOrderId)->first();
        $changeOrderStatusForUser->accepted=$orderReply->accepted;
        if ($request->accepted=='0')
        {
            $changeOrderStatusForUser->status = '6';
        }
        elseif ($request->accepted=='1')
        {
            $changeOrderStatusForUser->status = '1';
        }
        $changeOrderStatusForUser->save();

        $getUserId=Order::where('id',$request->id)->pluck('user_id')->first();

        $token = \App\User::where('id',$getUserId)->pluck('firebase_token')->toArray();


        $getJobDetailsId=JobDetails::where('order_id',$request->id)->first();

        if (empty($getJobDetailsId))
        {
            $jobDetail=new JobDetails();

            $jobDetail->order_id=$getOrderId;
            if ($request->accepted=='0')
            {
                $jobDetail->status = 6;
                $jobDetail->technician_id = null;

            } elseif ($request->accepted=='1')
            {
                $jobDetail->status = 1;
                $jobDetail->technician_id=$technician->id;
            }


            $jobDetail->user_id=$getUserId;
            $jobDetail->save();
        }else{
            if ($request->accepted=='0')
            {
                $getJobDetailsId->status = 6;
                $getJobDetailsId->technician_id = null;

            } elseif ($request->accepted=='1')
            {
                $getJobDetailsId->status = 1;
                $getJobDetailsId->technician_id =$technician->id;
            }
            $getJobDetailsId->save();
        }
        $userOrderSubscription=TechnicianOrder::where('order_id',$request->id)->pluck('order_id')->first();
        $getUserSubscriptionId=Order::where('id',$userOrderSubscription)->first();
        if(!empty($getUserSubscriptionId))
        {

            if ($request->accepted=='0')
            {
                $getUserSubscriptionId->status = '6';
            }elseif ($request->accepted=='1')
            {
                $getUserSubscriptionId->status = '1';
            }
            $getUserSubscriptionId->accepted = $orderReply->accepted;
            $getUserSubscriptionId->save();
        }

        if ($request->accepted=='1'){
            $technician->is_busy='1';
        }


        PushNotification::send_details($token, $orderReply->status,$changeOrderStatusForUser->order_number,trans('api.technician replied on order'),$changeOrderStatusForUser->id,1);


        $notification=new Notifications();
        $notification->order_id=$getOrderId;
        $notification->user_id=$user;
        $notification->text_en='technician replied on order';
        $notification->text_ar='قام الفني بالرد علي الطلب';
        $notification->save();


        $response=[
            'message'=>trans('api.technical report created successfully'),
            'status'=>'200',
        ];
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changeOrderStatus(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'status'=>'required',
        ]);
        if ($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $orderReply=TechnicianOrder::where('order_id',$request->id)->first();
        $orderReply->status = $request->status;
        $orderReply->save();
        $getOrderId=TechnicianOrder::where('order_id',$request->id)->pluck('order_id')->first();

        $changeOrderStatusForUser=Order::where('id',$getOrderId)->first();
        $changeOrderStatusForUser->status= $orderReply->status;
        $changeOrderStatusForUser->save();

        $jobDetail=JobDetails::where('order_id',$getOrderId)->first();
        $jobDetail->order_id=$getOrderId;
        $jobDetail->status =  $orderReply->status;
        $jobDetail->save();

        $userSubscription=UserSubscription::where('subscription_id',$changeOrderStatusForUser->subscription_id)->first();
        if (!empty($userSubscription))
        {
            $userSubscription->status =  $request->status;
            $userSubscription->save();
        }

        $notifications=Notifications::where('order_id',$getOrderId)->first();
        $notifications->text_en='order status changed successfully';
        $notifications->text_ar='تم تغيير حالة الطلب بنجاح';
        $notifications->save();

//        PushNotification::send($token,'order status changed successfully',1);
        $getUserID=Order::where('id',$getOrderId)->pluck('user_id')->first();

        $token = User::where('id',$getUserID)->pluck('firebase_token')->toArray();

//        dd($token);
//
//        $token = \App\User::where('firebase_token',$user)->pluck('firebase_token')->toArray();
//


        PushNotification::send_details($token, $orderReply->status,$changeOrderStatusForUser->order_number,trans('api.job status changed successfully'),$changeOrderStatusForUser->id,1);


        $response=[
            'message'=>trans('api.technical report created successfully'),
            'status'=>'200',
        ];
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    public function getServiceInProgress(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';

        $requests=\App\OrderSubcategory::where('order_id',$request->order_id)->get();
        $res_item = [];
        $res_list  = [];
        foreach ($requests as $res) {
            $res_item['id'] = $res->id;
            $subcategories=Subcategory::where('id',$res->subcategory_id)->select('name_'.$lang.' as name','price')->first();
            $res_item['name']=$subcategories->name;
            $res_item['price']=$subcategories->price;
            $res_item['quantity'] = $res->quantity;
            $res_item['total']=$subcategories->price*$res->quantity;
            $res_list[] = $res_item;
        }

        $response = [
            'message' => 'get data of user Requests successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }

    public function markServices(Request $request)
    {
//        $validator = Validator::make($request->all(), [
//            'finished' => 'required',
//        ]);
//        if ($validator->fails()) {
//            $response = [
//                'message' => 'failed to booking',
//                'status' => 404,
//            ];
//        }
        for($i=0;$i<count($request->subcategory_id);$i++)
        {
            $requests= OrderSubcategory::where('id',$request->subcategory_id[$i])->first();
            $requests->update([
                'finished'=>'1'
            ]);
//            $requests->finished=$request->finished[$i];
        }

        $response = [
            'message' => 'Reservation request sent successfully',
            'status' => 200,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }




}
