<?php

namespace App\Http\Controllers\Api\Technician;

use App\Http\Controllers\Api\BaseController;
use App\Order;
use App\OrderPriceAmount;
use App\PushNotification;
use App\Technician;
use App\TechnicianNote;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\TechnicianOrder;
use App\JobDetails;

class OrderPriceAmountController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function enterPriceAmount(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->pluck('id')->first();
        $order=Order::where('id',$request->order_id)->first();
        if (!$user) {
            return $response=[
                'success'=>403,
                'message'=>trans('api.please login first'),
            ];
        }

        $validator = Validator::make($request->all(), [
            'price_amount'=>'required',
            'order_id'=>'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $priceAmount=new OrderPriceAmount();
        $priceAmount->order_id=$order->id;
        $priceAmount->price_amount = $request->price_amount;
        $priceAmount->save();

         $orderStatus= TechnicianOrder::where('order_id',$request->order_id)->first();
         $orderStatus->status = '5';
         $orderStatus->save();

        $changeOrderStatusForUser=Order::where('id',$request->order_id)->first();
        $changeOrderStatusForUser->status = '5';
        $changeOrderStatusForUser->save();

        $jobDetail=JobDetails::where('order_id',$request->order_id)->first();

        $jobDetail->status = '5';
        $jobDetail->save();


        $token = User::where('id',$changeOrderStatusForUser->user_id)->pluck('firebase_token')->toArray();



        PushNotification::send_details($token,  $changeOrderStatusForUser->status,$changeOrderStatusForUser->order_number,trans('api.job successfully posted'),$changeOrderStatusForUser->id,1);


        $response=[
            'message'=>trans('api.amount price entered successfully'),
            'status'=>200,
        ];
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
