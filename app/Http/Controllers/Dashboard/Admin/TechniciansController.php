<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Category;
use App\Order;
use App\OrderPriceAmount;
use App\OrderSubcategory;
use App\Subcategory;
use App\Technician;
use App\TechnicianOrder;
use App\Unit;
use App\User;
use App\UserTechnicianSubscription;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class TechniciansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.views.technicians.index')->with('users',User::where('user_type','technician')->get());

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $units = Unit::all();
        $subcategories=Subcategory::all();
        return view('dashboard.views.technicians.create',compact('categories','subcategories',
        'units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'password'=>'required',
            'phone'=>'required',
            'category_id' => 'required',
            'unit_id'=> 'required',
            'subcategory_id' => 'required',
            'address'=>'required',

        ]);
        $user=User::where('email',$request->email)->orWhere('phone',$request->phone)->exists();
        if ($user){
            return redirect()->route('technicians.index')->with('successMsg','User Created Before');
        }
        $jwt_token = Str::random(25);
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'phone'=>$request->phone,
            'user_type'=>'technician',
            'user_status'=>'1',
            'status'=>'1',
            'verify_code'=>'0',
            'jwt_token'=>$jwt_token,
            'lat'=>'0',
            'lng'=>'0',
            'facebook_token'=>'0',
            'google_token'=>'0',
            'address'=>$request->address,
            'firebase_token'=>'0'
        ]);
        $technician=New Technician();
        $technician->user_id=$user->id;
        $technician->category_id = $request->category_id;
        $technician->unit_id = $request->unit_id;
        $technician->subcategory_id=$request->subcategory_id;
        $technician->is_busy='0';
        if ( $technician->save())
        {
            return redirect()->route('technicians.index')->with('successMsg','technician Successfully Created');
        }
        return redirect()->route('technicians.create')->with('successMsg','sorry something went wrong');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories = Category::all();
        $units = Unit::all();
        $subcategories = Subcategory::all();
        $user = User::find($id);
        $technician=Technician::where('user_id',$id)->first();

        $technicianOrders=TechnicianOrder::where('technician_id',$technician->id)->pluck('order_id')->first();

        $technicianCollectedMoney =  OrderPriceAmount::where('order_id',$technicianOrders)
            ->whereYear('created_at', Carbon::now()->year)
            ->select(DB::raw("YEAR(created_at) year,MONTHNAME(created_at) month,SUM(price_amount) sum"))
            ->groupby('month')
            ->get();

        $getTechnicianOrders=TechnicianOrder::where('technician_id',$technician->id)->get();

        $getOrdersSubcategoriesIds=OrderSubcategory::where('order_id',$technicianOrders)->pluck('subcategory_id')->first();
        $getUnitsIds=Subcategory::where('id',$getOrdersSubcategoriesIds)->pluck('unit_id')->first();
        $getCategoriesIds=Unit::where('id',$getUnitsIds)->pluck('category_id')->first();
        $getCategories=Category::where('id',$getCategoriesIds)->get();

        $getTotalPrice=OrderSubcategory::where('order_id',$technicianOrders)->get();


        $getTechnicianSubscriptions=UserTechnicianSubscription::where('technician_id',$technician->id)->get();

//        dd($getTechnicianSubscriptions);



        return view('dashboard.views.technicians.show',compact(
   'user','technician',
            'units', 'categories',
            'subcategories','technicianCollectedMoney',
            'getTechnicianOrders','getCategories','getTotalPrice','getTechnicianSubscriptions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all();
        $units = Unit::all();
        $subcategories = Subcategory::all();
        $user = User::find($id);
        $technician=Technician::where('user_id',$id)->first();
//        dd($technician);
        return view('dashboard.views.technicians.edit',compact('user','technician',
            'units',
            'categories','subcategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $technician=Technician::where('user_id',$id)->first();

        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password =$request->password;
        $user->address =$request->address;
        $technician->category_id = $request->category_id;
        $technician->unit_id = $request->unit_id;
        $technician->subcategory_id=$request->subcategory_id;
        $technician->is_busy=$request->is_busy;

        $user->save();

        $technician->save();

            return redirect()->route('technicians.index')->with('successMsg','User Successfully Updated');


        $users=User::where('phone',$request->phone)->orWhere('email',$request->email)->exists();
        if($users)
        {
            return redirect()->route('technicians.index')->with('successMsg','User Created Before');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $technician=Technician::where('user_id',$id)->first();
        $user->delete();
        $technician->delete();
        return redirect()->back()->with('successMsg','technician Successfully Delete');
    }

    public function updateStatus(Request $request)
    {
        $user = User::findOrFail($request->id);
        $user->user_status = $request->user_status;
        if($user->save()){
            return 1;
        }
        return 0;
    }

    public function technicianAvailability(Request $request)
    {
        $technician = Technician::findOrFail($request->id);
        $technician->is_busy = $request->is_busy;
        if($technician->save()){
            return 1;
        }
        return 0;
    }
}
