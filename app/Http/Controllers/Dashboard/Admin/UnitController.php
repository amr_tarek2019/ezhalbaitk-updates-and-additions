<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Category;
use App\Currency;
use App\Unit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $units = Unit::all();
        return view('dashboard.views.units.index', compact('units'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('dashboard.views.units.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'category' => 'required',
            'name_en' => 'required',
            'name_ar' => 'required',
            'image' => 'required|mimes:jpeg,jpg,bmp,png',
            'warranty_period'=>'required',
            'period_en'=>'required',
            'period_ar'=>'required',
            'warranty_text_en'=>'required',
            'warranty_text_ar'=>'required',
        ]);
        $unit = new Unit();
        $unit->category_id = $request->category;
        $unit->name_en = $request->name_en;
        $unit->name_ar = $request->name_ar;
        $unit->image = $request->image;
        $unit->warranty_period=$request->warranty_period;
        $unit->period_en=$request->period_en;
        $unit->period_ar=$request->period_ar;
        $unit->warranty_text_en=$request->warranty_text_en;
        $unit->warranty_text_ar=$request->warranty_text_ar;
        $unit->save();
        return redirect()->route('units.index')->with('successMsg','Profile Updated Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $unit = Unit::find($id);
        $categories = Category::all();
        return view('dashboard.views.units.edit',compact('categories','unit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $unit = Unit::find($id);
        $unit->category_id = $request->category;
        $unit->name_en = $request->name_en;
        $unit->name_ar = $request->name_ar;
        $unit->image = $request->image;
        $unit->warranty_period=$request->warranty_period;
        $unit->period_en=$request->period_en;
        $unit->period_ar=$request->period_ar;
        $unit->warranty_text_en=$request->warranty_text_en;
        $unit->warranty_text_ar=$request->warranty_text_ar;
        $unit->save();
        return redirect()->route('units.index')->with('successMsg','Profile Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unit = Unit::find($id);
        $unit->delete();
        return redirect()->route('units.index')->with('successMsg','Profile Updated Successfully');
    }

    public function updateStatus(Request $request)
    {
        $unit = Unit::findOrFail($request->id);
        $unit->status = $request->status;
        if($unit->save()){
            return 1;
        }
        return 0;
    }

}
