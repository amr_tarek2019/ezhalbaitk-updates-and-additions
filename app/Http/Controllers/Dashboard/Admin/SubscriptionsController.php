<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Category;
use App\Package;
use App\Subscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscriptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $subscriptions=Subscription::all();
      return view('dashboard.views.subscriptions.index',compact('subscriptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::all();
        $packages=Package::all();
        return view('dashboard.views.subscriptions.create',compact('categories','packages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'category' => 'required',
            'package' => 'required',
            'months' => 'required',
            'price' => 'required',
        ]);
        $subscription = new Subscription();
        $subscription->category_id = $request->category;
        $subscription->package_id = $request->package;
        $subscription->months = $request->months;
        $subscription->price = $request->price;
        $subscription->save();
        return redirect()->route('subscriptions.index')->with('successMsg','Category Updated Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subscription=Subscription::find($id);
        $packages=Package::all();
        $categories=Category::all();
        return view('dashboard.views.subscriptions.edit',compact('subscription','packages','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subscription = Subscription::find($id);
        $subscription->category_id = $request->category;
        $subscription->package_id = $request->package;
        $subscription->months = $request->months;
        $subscription->price = $request->price;
        $subscription->save();
        return redirect()->route('subscriptions.index')->with('successMsg','Category Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subscription=Subscription::find($id);
        $subscription->delete();
        return redirect()->back()->with('successMsg','Category Successfully Delete');
    }

    public function updateStatus(Request $request)
    {
        $subscription = Subscription::findOrFail($request->id);
        $subscription->status = $request->status;
        if($subscription->save()){
            return 1;
        }
        return 0;
    }
}
