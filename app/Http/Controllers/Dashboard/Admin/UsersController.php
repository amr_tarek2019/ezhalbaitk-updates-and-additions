<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Order;
use App\OrderPriceAmount;
use App\User;
use App\UserSubscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.views.users.index')->with('users',User::where('user_type','user')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $this->validate($request,[
//            'name' => 'required',
//            'email' => 'required',
//            'password'=>'required',
//            'phone'=>'required',
//        ]);
//        $user=User::where('email',$request->email)->orWhere('phone',$request->phone)->exists();
//        if ($user){
//            return redirect()->route('user.index')->with('successMsg','User Created Before');
//        }
//
//        $user = User::create([
//            'name' => $request->name,
//            'email' => $request->email,
//            'password' => $request->password,
//            'phone'=> $request->phone,
//            'user_type'=>'user',
//            'user_status'=>'1',
//            'status'=>'0',
//            'verify_code'=>'0',
//            'jwt_token'=>'0',
//            'latitude'=>'0',
//            'longitude'=>'0',
//            'facebook_token'=>'0',
//            'google_token'=>'0',
//            'address'=>'0',
//            'firebase_token'=>'0'
//        ]);
//        //dd($user);
//
//        if ( $user->save())
//        {
//            return redirect()->route('user.index')->with('successMsg','User Successfully Created');
//        }
//        return redirect()->route('user.create')->with('successMsg','sorry something went wrong');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        $userSubscriptions=UserSubscription::orderBy('id', 'desc')->where('user_id',$id)->get();

        $userOrders=Order::orderBy('id', 'desc')->where('user_id',$id)->where('status','0')->where('accepted','0')->get();

        $userFinishedOrders=Order::orderBy('id', 'desc')->where('user_id',$id)->where('status','5')->where('accepted','1')->get();

        $getUserSubscriptionsOrders=UserSubscription::where('user_id',$id)->pluck('subscription_id')->first();
        $userSubscriptionsOrders=Order::where('subscription_id',$getUserSubscriptionsOrders)->get();
        



        $getOrdersPriceAmounts=Order::where('user_id',$id)->pluck('id')->first();
        $priceAmounts=OrderPriceAmount::where('order_id',$getOrdersPriceAmounts)->get();


        return view('dashboard.views.users.show',compact('user','userSubscriptions',
            'userOrders', 'userFinishedOrders',
            'userSubscriptionsOrders','priceAmounts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        $user = User::find($id);
//        return view('dashboard.views.users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->password =$request->password;
        $user->save();
            return redirect()->route('users.show',['id' => $user->id])->with('successMsg','User Successfully Updated');
        $user=User::where('phone',$request->phone)->orWhere('email',$request->email)->exists();
        if($user)
        {
            return redirect()->route('users.show',['id' => $user->id])->with('successMsg','User Created Before');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->back()->with('successMsg','User Successfully Delete');
    }

    public function updateStatus(Request $request)
    {
        $user = User::findOrFail($request->id);
        $user->user_status = $request->user_status;
        if($user->save()){
            return 1;
        }
        return 0;
    }
}
