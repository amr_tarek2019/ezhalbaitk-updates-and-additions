<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\About;
use App\Setting;
use App\Settings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.views.settings.index')->with('settings', Setting::first());

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $setting = Setting::first();
        $setting->text_en =  $request->text_en;
        $setting->text_ar =  $request->text_ar;
        $setting->image=$request->image;
        $setting->facebook=$request->facebook;
        $setting->google=$request->google;
        $setting->terms_conditions_en =  $request->terms_conditions_en;
        $setting->terms_conditions_ar =  $request->terms_conditions_ar;
        if ($setting->save())
        {
            return redirect()->route('settings')->with('successMsg','Settings Updated Successfully');
        }
        return redirect()->route('settings')->with('successMsg','sorry something went wrong');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
