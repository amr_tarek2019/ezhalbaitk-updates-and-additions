<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TechnicianOrder extends Model
{
    protected $table='technicians_orders';
    protected $fillable=['order_id', 'technician_id', 'order_number', 'status', 'accepted'];
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toDateString();
    }
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function order()
    {
        return $this->belongsTo('App\Order','order_id');
    }

    public function orderSubcategory()
    {
        return $this->belongsTo('App\OrderSubcategory','order_id');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\Subcategory','subcategory_id');
    }


}
