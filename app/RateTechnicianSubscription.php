<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class RateTechnicianSubscription extends Model
{
    protected $table='rate_technicians_subscriptions';
    protected $fillable=[ 'user_id', 'technician_id', 'user_subscription_id', 'rate', 'review'];

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toDateString();
    }
}
