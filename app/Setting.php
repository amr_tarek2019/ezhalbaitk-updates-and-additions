<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table='settings';
    protected $fillable=[
        'image', 'text_en', 'text_ar',
        'facebook', 'google', 'terms_conditions_en',
        'terms_conditions_ar'];

    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('uploads/settings/'.$value);
        } else {
            return asset('uploads/user/profile/default.png');
        }
    }

    public function setImageAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/settings/'),$imageName);
            $this->attributes['image']=$imageName;
        }
    }
}
