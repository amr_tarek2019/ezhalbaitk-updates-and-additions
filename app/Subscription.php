<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $table='subscriptions';
    protected $fillable=['category_id', 'package_id', 'months', 'price','status','currency'];

    public function subscriptionsCategories()
    {
        return $this->hasManyThrough(Subscription::class, Category::class);
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toDateString();
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function package()
    {
        return $this->belongsTo('App\Package','package_id');
    }
}
