<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone','address', 'lat', 'lng', 'social_type', 'social_token',
        'jwt_token','firebase_token','user_status', 'status','verify_code', 'user_type','image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       'remember_token','created_at','updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



    public function setImageAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/user/profile/'),$imageName);
            $this->attributes['image']=$imageName;
        }
    }
    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('uploads/user/profile/'.$value);
        } else {
            return asset('uploads/user/profile/default.png');
        }
    }
    public function setPasswordAttribute($password)
    {
        return $this->attributes['password'] = bcrypt($password);
    }

      public function technicians()
    {
        return $this->hasMany('App\Technician');
    }

      public function rateTechnicians()
    {
        return $this->hasMany('App\RateTechnician');
    }

      public function subcategories()
    {
        return $this->hasMany('App\Subcategory');
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toDateString();
    }

    public function subscription()
    {
        return $this->belongsTo('App\Subscription','subscription_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function package()
    {
        return $this->belongsTo('App\Package');
    }

    public function city()
    {
        return $this->belongsTo('App\City','city_id');
    }
//
//    public function setPhoneAttribute($phone)
//    {
//        return $this->attributes['phone'] = ltrim($phone, '+974');
//    }
}
